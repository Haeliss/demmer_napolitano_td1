package main;

import dao.*;
import metier.*;

import java.util.Scanner;

public class ExecConnexion{
	static Scanner sc = new Scanner(System.in);
	static DAOFactory daos = DAOFactory.getDAOFactory("MySQL"); //CHANGER POUR ListeMemoire /!\/!\/!\
	static boolean test=false;
	static boolean test2=false;
	// Variable periodicite //
	static int id;
	static String libel;
	// Variable revue //
	static int idrevue;
	static String titre, desc, visuel;
	static float tarif;
	// Variable client //
	static int idclient;
	static String nom, prenom, norue, voie, postale, ville, pays;
	// Variable abonnement //
	static String datedebut = null, datefin=null;
	// Variable Duree //
	static int idduree;
	// Variable Formule //
	static float reduc;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while(test==false){
		test2=false;
		// Menu demarrage //
		System.out.println("Que voulez-vous modifier ?");
		System.out.println("	1)Les periodicites");
		System.out.println("	2)Les revues");
		System.out.println("	3)Les clients");
		System.out.println("	4)Les abonnements");
		System.out.println("	5)Les Durees");
		System.out.println("	6)Les Formules");
		System.out.println("	7)Quitter");
		System.out.println("Choisissez le numero correspondant.");
		int nb = sc.nextInt();
		if(nb==1){
			// Menu periodicite //
			MenuPeriodicite();
		}
		else if(nb==2){
			// Menu revue //
			MenuRevue();
		}
		else if(nb==3){
			// Menu client //
			MenuClient();
		}
		else if(nb==4){
			// Menu abonnement //
			MenuAbonnement();
		}
		else if(nb==5){
			// Menu Duree //
			MenuDuree();
		}

		else if(nb==6){
			// Menu Formule //
			MenuFormule();	
		}
		else if(nb==7){
			test=true;
		}
		else{
			System.out.println("Choisissez un chiffre correspondant � votre choix.");
		}
		}
		System.out.println("Fin du programme");
	}
	
	public static void MenuPeriodicite(){
				while(test2==false){
				System.out.println("	--Periodicite--");
				System.out.println("Que voulez-vous faire ?");
				System.out.println("	1)Ajouter");
				System.out.println("	2)Modifier");
				System.out.println("	3)Supprimer");
				System.out.println("	4)Retour");
				System.out.println("Choisissez le numero correspondant.");
				int nb2=sc.nextInt();
				if(nb2==1){
					// Menu ajout periodicite //
					AddPeriodicite();
				}
				else if(nb2==2){
					// Menu modif. periodicite //
					ModPeriodicite();
				}
				else if(nb2==3){
					// Menu suppr. periodicite //
					SuppPeriodicite();
				}
				else if(nb2==4){
					test2=true;
				}
				else{
					System.out.println("Choisissez un chiffre correspondant � votre choix.");
				}
				}
	}
		public static void AddPeriodicite(){
		System.out.println("	--Ajout--");
		System.out.println("Saisissez l'id:");
		id=sc.nextInt();
		System.out.println("Saisissez le libelle: ");
		sc.nextLine();
		libel=sc.nextLine();
		Periodicite p= new Periodicite(id,libel);
		daos.getPeriodiciteDAO().create(p);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
		public static void ModPeriodicite(){
		System.out.println("	--Modification--");
		System.out.println("Saisissez l'id de la periodicite que vous souhaitez modifier:");
		id=sc.nextInt();
		System.out.println("Saisissez le nouveau libelle: ");
		sc.nextLine();
		libel=sc.nextLine();
		Periodicite p = daos.getPeriodiciteDAO().GetById(id);
		p.setLibel(libel);
		daos.getPeriodiciteDAO().update(p);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
		public static void SuppPeriodicite(){
		System.out.println("	--Suppression--");
		System.out.println("Saisissez l'id:");
		id=sc.nextInt();
		Periodicite p= daos.getPeriodiciteDAO().GetById(id);
		daos.getPeriodiciteDAO().delete(p);
		System.out.println("effectuee avec succes !");
		test2=true;
	}

	public static void MenuRevue(){
		while(test2==false){
			System.out.println("	--Revue--");
			System.out.println("Que voulez-vous faire ?");
			System.out.println("	1)Ajouter");
			System.out.println("	2)Modifier");
			System.out.println("	3)Supprimer");
			System.out.println("	4)Retour");
			System.out.println("Choisissez le numero correspondant.");
			int nb2=sc.nextInt();
			if(nb2==1){
				// Menu ajout revue //
				AddRevue();
			}
			else if(nb2==2){
				// Menu modif. revue //
				ModRevue();
			}
			else if(nb2==3){
				// Menu suppr. revue //
				SuppRevue();
			}
			else if(nb2==4){
				test2=true;
			}
			else{
				System.out.println("Choisissez un chiffre correspondant � votre choix.");
			}
			}
	}
		public static void AddRevue(){
		System.out.println("	--Ajout--");
		System.out.println("Saisissez l'id:");
		idrevue=sc.nextInt();
		System.out.println("Saisissez le titre:");
		sc.nextLine();
		titre=sc.nextLine();
		System.out.println("Saisissez la description:");
		desc=sc.nextLine();
		System.out.println("Saisissez le tarif:");
		tarif=sc.nextFloat();
		System.out.println("Saisissez le visuel:");
		sc.nextLine();
		visuel=sc.nextLine();
		System.out.println("Saisissez l'id de la periodicite correspondante:");
		id=sc.nextInt();
		Revue r= new Revue(idrevue,titre,desc,visuel,tarif,daos.getPeriodiciteDAO().GetById(id));
		daos.getRevueDAO().create(r);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
		public static void ModRevue(){
		System.out.println("	--Modification--");
		System.out.println("Saisissez l'id de la revue que vous souhaitez modifier :");
		idrevue=sc.nextInt();
		System.out.println("Saisissez le titre:");
		sc.nextLine();
		titre=sc.nextLine();
		System.out.println("Saisissez la description:");
		desc=sc.nextLine();
		System.out.println("Saisissez le tarif:");
		tarif=sc.nextFloat();
		System.out.println("Saisissez le visuel:");
		sc.nextLine();
		visuel=sc.nextLine();
		System.out.println("Saisissez l'id de la periodicite correspondante:");
		id=sc.nextInt();
		Revue r= daos.getRevueDAO().GetById(idrevue);
		r.setTitre(titre);r.setDesc(desc);r.setTarif(tarif);r.setVisuel(visuel);
		r.setPeriod(daos.getPeriodiciteDAO().GetById(id));
		daos.getRevueDAO().update(r);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
		public static void SuppRevue(){
		System.out.println("	--Suppression--");
		System.out.println("Saisissez l'id:");
		idrevue=sc.nextInt();
		Revue r= daos.getRevueDAO().GetById(idrevue);
		daos.getRevueDAO().delete(r);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
	
	public static void MenuClient(){
		while(test2==false){
			System.out.println("	--Client--");
			System.out.println("Que voulez-vous faire ?");
			System.out.println("	1)Ajouter");
			System.out.println("	2)Modifier");
			System.out.println("	3)Supprimer");
			System.out.println("	4)Retour");
			System.out.println("Choisissez le numero correspondant.");
			int nb2=sc.nextInt();
			if(nb2==1){
				// Menu ajout client //
				AddClient();
			}
			else if(nb2==2){
				// Menu modif. client //
				ModClient();
			}
			else if(nb2==3){
				// Menu suppr. client //
				SuppClient();
			}
			else if(nb2==4){
				test2=true;
			}
			else{
				System.out.println("Choisissez un chiffre correspondant � votre choix.");
			}
			}
	}
		public static void AddClient(){
		System.out.println("	--Ajout--");
		System.out.println("Saisissez l'id:");
		idclient=sc.nextInt();
		System.out.println("Saisissez le nom:");
		sc.nextLine();
		nom=sc.nextLine();
		System.out.println("Saisissez le prenom:");
		prenom=sc.nextLine();
		System.out.println("Saisissez le numero de rue:");
		norue=sc.nextLine();
		System.out.println("Saisissez la voie:");
		voie=sc.nextLine();
		System.out.println("Saisissez le code postal:");
		postale=sc.nextLine();
		System.out.println("Saisissez la ville:");
		ville=sc.nextLine();
		System.out.println("Saisissez le pays:");
		pays=sc.nextLine();
		Client c= new Client(idclient,nom,prenom,norue,voie,postale,ville,pays);
		daos.getClientDAO().create(c);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
		public static void ModClient(){
		System.out.println("	--Modification--");
		System.out.println("Saisissez l'id:");
		idclient=sc.nextInt();
		System.out.println("Saisissez le nom:");
		sc.nextLine();
		nom=sc.nextLine();
		System.out.println("Saisissez le prenom:");
		prenom=sc.nextLine();
		System.out.println("Saisissez le numero de rue:");
		norue=sc.nextLine();
		System.out.println("Saisissez la voie:");
		voie=sc.nextLine();
		System.out.println("Saisissez le code postal:");
		postale=sc.nextLine();
		System.out.println("Saisissez la ville:");
		ville=sc.nextLine();
		System.out.println("Saisissez le pays:");
		pays=sc.nextLine();
		Client c = daos.getClientDAO().GetById(idclient);
		c.setNom(nom);c.setPrenom(prenom);c.setNorue(norue);c.setVoie(voie);c.setPostale(postale);c.setVille(ville);c.setPays(pays);
		daos.getClientDAO().update(c);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
		public static void SuppClient(){
		System.out.println("	--Suppression--");
		System.out.println("Saisissez l'id:");
		idclient=sc.nextInt();
		Client c= daos.getClientDAO().GetById(idclient);
		daos.getClientDAO().delete(c);
		System.out.println("effectuee avec succes !");
		test2=true;
	}
	
	public static void MenuAbonnement(){
		while(test2==false){
			System.out.println("	--Abonnement--");
			System.out.println("Que voulez-vous faire ?");
			System.out.println("	1)Ajouter");
			System.out.println("	2)Modifier");
			System.out.println("	3)Supprimer");
			System.out.println("	4)Retour");
			System.out.println("Choisissez le numero correspondant.");
			int nb2=sc.nextInt();
			if(nb2==1){
				// Menu ajout abonnement //
				AddAbonnement();
			}
			else if(nb2==2){
				// Menu modif. abonnement //
				ModAbonnement();
			}
			else if(nb2==3){
				// Menu suppr. abonnement //
				SuppAbonnement();
			}
			else if(nb2==4){
				test2=true;
			}
			else{
				System.out.println("Choisissez un chiffre correspondant � votre choix.");
			}
			}
	}
		public static void AddAbonnement(){
			System.out.println("	--Ajout--");
			System.out.println("Saisissez l'id du client:");
			idclient=sc.nextInt();
			System.out.println("Saisissez l'id de la revue:");
			idrevue=sc.nextInt();
			System.out.println("Saisissez l'id de la duree choisie:");
			idduree=sc.nextInt();
			System.out.println("Saisissez la date de debut (AAAA-MM-JJ):");
			sc.nextLine();
			datedebut=sc.nextLine();
			if (!datedebut.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
				 System.out.println("Erreur format.");
				 test2=true;
			}
			System.out.println("Saisissez la date de fin (AAAA-MM-JJ):");
			datefin=sc.nextLine();
			if(datefin.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
				daos.getAbonnementDAO().createAbonnement(daos.getClientDAO().GetById(idclient),daos.getRevueDAO().GetById(idrevue),datedebut,datefin,daos.getDureeDAO().GetById(idduree));
				System.out.println("effectuee avec succes !");
			}
			else {
			    System.out.println("Erreur format.");
			    test2=true;
			}
			test2=true;
		}
		public static void ModAbonnement(){
			System.out.println("	--Modification--");
			System.out.println("Saisissez l'id du client:");
			idclient=sc.nextInt();
			System.out.println("Saisissez l'id de la revue:");
			idrevue=sc.nextInt();
			System.out.println("Saisissez l'id de la duree choisie:");
			idduree=sc.nextInt();
			System.out.println("Saisissez la date de debut (AAAA-MM-JJ):");
			sc.nextLine();
			datedebut=sc.nextLine();
			if (!datedebut.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
				 System.out.println("Erreur format.");
				 test2=true;
			}
			System.out.println("Saisissez la date de fin (AAAA-MM-JJ):");
			datefin=sc.nextLine();
			if(datefin.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
				daos.getAbonnementDAO().updateAbonnement(daos.getClientDAO().GetById(idclient),daos.getRevueDAO().GetById(idrevue),datedebut,datefin,daos.getDureeDAO().GetById(idduree));
				System.out.println("effectuee avec succes !");
			}
			else {
			    System.out.println("Erreur format.");
			    test2=true;
			}
			test2=true;
		}
		public static void SuppAbonnement(){
			System.out.println("	--Suppression--");
			System.out.println("Saisissez l'id du client:");
			idclient=sc.nextInt();
			System.out.println("Saisissez l'id de la revue:");
			idrevue=sc.nextInt();
			daos.getAbonnementDAO().deleteAbonnement(daos.getClientDAO().GetById(idclient),daos.getRevueDAO().GetById(idrevue));
			System.out.println("effectuee avec succes !");
			test2=true;
		}
	
	public static void MenuDuree(){
		while(test2==false){
			System.out.println("	--Duree--");
			System.out.println("Que voulez-vous faire ?");
			System.out.println("	1)Ajouter");
			System.out.println("	2)Modifier");
			System.out.println("	3)Supprimer");
			System.out.println("	4)Retour");
			System.out.println("Choisissez le numero correspondant.");
			int nb2=sc.nextInt();
			if(nb2==1){
				// Menu ajout Duree //
				AddDuree();
			}
			else if(nb2==2){
				// Menu modif. Duree //
				ModDuree();
			}
			else if(nb2==3){
				// Menu suppr. Duree //
				SuppDuree();
			}
			else if(nb2==4){
				test2=true;
			}
			}
	}
		public static void AddDuree(){
			System.out.println("	--Ajout--");
			System.out.println("Saisissez l'id:");
			idduree=sc.nextInt();
			System.out.println("Saisissez le Libelle de la formule:");
			sc.nextLine();
			libel=sc.nextLine();
			Duree d= new Duree(idduree,libel);
			daos.getDureeDAO().create(d);
			System.out.println("effectuee avec succes !");
			test2=true;
		}
		public static void ModDuree(){
		System.out.println("	--Modification--");
		System.out.println("Saisissez l'id:");
		idduree=sc.nextInt();
		System.out.println("Saisissez le Libelle de la formule:");
		sc.nextLine();
		libel=sc.nextLine();
		Duree d= daos.getDureeDAO().GetById(idduree);
		d.setLibelleFormule(libel);
		daos.getDureeDAO().update(d);
		System.out.println("effectuee avec succes !");
		test2=true;
		}
		public static void SuppDuree(){
			System.out.println("	--Suppression--");
			System.out.println("Saisissez l'id:");
			idduree=sc.nextInt();
			Duree d= daos.getDureeDAO().GetById(idduree);
			daos.getDureeDAO().delete(d);
			System.out.println("effectuee avec succes !");
			test2=true;
		}
	
	public static void MenuFormule(){
		while(test2==false){
			System.out.println("	--Formules--");
			System.out.println("Que voulez-vous faire ?");
			System.out.println("	1)Ajouter");
			System.out.println("	2)Modifier");
			System.out.println("	3)Supprimer");
			System.out.println("	4)Retour");
			System.out.println("Choisissez le numero correspondant.");
			int nb2=sc.nextInt();
			if(nb2==1){
				// Menu ajout Formule //
				AddFormule();
			}
			else if(nb2==2){
				// Menu modif. Formule //
				ModFormule();
			}
			else if(nb2==3){
				// Menu  suppr. Formule //
				SuppFormule();
			}
			else if(nb2==4){
				test2=true;
			}
			}
	}
		public static void AddFormule(){
			System.out.println("	--Ajout--");
			System.out.println("Saisissez l'id de la duree:");
			idduree=sc.nextInt();
			System.out.println("Saisissez l'id de la revue:");
			idrevue=sc.nextInt();
			System.out.println("Saisissez le montant de la reduction:");
			reduc=sc.nextFloat();
			daos.getFormuleDAO().createFormule(daos.getRevueDAO().GetById(idrevue),daos.getDureeDAO().GetById(idduree) , reduc);
			System.out.println("effectuee avec succes !");
			test2=true;
		}
		public static void ModFormule(){
			System.out.println("	--Modification--");
			System.out.println("Saisissez l'id de la duree:");
			idduree=sc.nextInt();
			System.out.println("Saisissez l'id de la revue:");
			idrevue=sc.nextInt();
			System.out.println("Saisissez le montant de la reduction:");
			reduc=sc.nextFloat();
			daos.getFormuleDAO().updateFormule(daos.getRevueDAO().GetById(idrevue),daos.getDureeDAO().GetById(idduree) , reduc);
			System.out.println("effectuee avec succes !");
			test2=true;
		}
		public static void SuppFormule(){
			System.out.println("	--Suppression--");
			System.out.println("Saisissez l'id de la duree:");
			idduree=sc.nextInt();
			System.out.println("Saisissez l'id de la revue:");
			idrevue=sc.nextInt();
			daos.getFormuleDAO().deleteFormule(daos.getRevueDAO().GetById(idrevue),daos.getDureeDAO().GetById(idduree));
			System.out.println("effectuee avec succes !");
			test2=true;
		}
	
}