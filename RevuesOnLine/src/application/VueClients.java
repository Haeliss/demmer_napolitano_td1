package application;

import java.net.URL;

import controller.GestionClientController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueClients extends Stage {
	public VueClients() {
		try {
			final URL fxmlURL = getClass().getResource("fxml/GestionClient.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			final VBox node = (VBox)fxmlLoader.load();
            Scene scene = new Scene(node);
            this.setScene(scene);
            this.setTitle("Gestion des Clients");
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		this.initModality(Modality.APPLICATION_MODAL);
		GestionClientController controleur = fxmlLoader.getController();
		controleur.setVue(this);
		this.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
