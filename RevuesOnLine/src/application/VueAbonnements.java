package application;

import java.net.URL;

import controller.GestionAbonnementController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueAbonnements extends Stage {
	public VueAbonnements() {
		try {
			final URL fxmlURL = getClass().getResource("fxml/GestionAbonnement.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			final VBox node = (VBox)fxmlLoader.load();
            Scene scene = new Scene(node);
            this.setScene(scene);
            this.setTitle("Gestion des Abonnements");
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		this.initModality(Modality.APPLICATION_MODAL);
		GestionAbonnementController controleur = fxmlLoader.getController();
		controleur.setVue(this);
		this.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}