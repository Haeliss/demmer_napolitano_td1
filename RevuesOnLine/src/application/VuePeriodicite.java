package application;

import java.net.URL;

import controller.gestionperiodiciteController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VuePeriodicite extends Stage {
	public VuePeriodicite() {
		try {
			final URL fxmlURL = getClass().getResource("fxml/gestionperiodicite.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			final VBox node = (VBox)fxmlLoader.load();
            Scene scene = new Scene(node);
            this.setScene(scene);
            this.setTitle("Gestion des périodicitées");
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		this.initModality(Modality.APPLICATION_MODAL);
		gestionperiodiciteController controleur = fxmlLoader.getController();
		controleur.setVue(this);
		this.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
