package application;

import java.net.URL;

import controller.gestiondureeController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueDuree extends Stage {
	public VueDuree() {
		try {
			final URL fxmlURL = getClass().getResource("fxml/gestionduree.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			final VBox node = (VBox)fxmlLoader.load();
            Scene scene = new Scene(node);
            this.setScene(scene);
            this.setTitle("Gestion des Dur�es");
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		this.initModality(Modality.APPLICATION_MODAL);
		gestiondureeController controleur = fxmlLoader.getController();
		controleur.setVue(this);
		this.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
