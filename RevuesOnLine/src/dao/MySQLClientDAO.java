package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metier.*;

public class MySQLClientDAO implements ClientDAO {
	private static MySQLClientDAO instance;
	public MySQLClientDAO(){}
	public static MySQLClientDAO getInstance() {
		if (instance==null) {
		instance = new MySQLClientDAO();
		}
		return instance;
		}
	
	public Client GetById(int id){
		HashMap<Revue, DatesAbonnement> lesAbos= new HashMap<>();
		Client client;Duree duree;DatesAbonnement datesAbo;Revue revue;
		int idClient=0, idDuree=0, idRevue=0;
		String nom=null, prenom=null, noRue = null, voie=null, postale=null, ville=null, pays=null, dateDebut=null, dateFin=null;
		 Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Client, Abonnement where id_client=?");
				 req.setInt(1,id );
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 idClient=res.getInt(1);
					 nom=res.getString(2);prenom=res.getString(3); noRue=res.getString(4); 
					 voie=res.getString(5); postale=res.getString(6); ville=res.getString(7); pays=res.getString(8);
					 idRevue=res.getInt(10);dateDebut=res.getString(11);dateFin=res.getString(12);idDuree=res.getInt(13);
				 }
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 duree=MySQLDureeDAO.getInstance().GetById(idDuree);
		 datesAbo= new DatesAbonnement(dateDebut, dateFin,duree);
		 revue=MySQLRevueDAO.getInstance().GetById(idRevue);
		 lesAbos.put(revue, datesAbo);
		 client=new Client(idClient, nom,prenom,noRue,voie,postale,ville,pays,lesAbos);
		 return client;
	}
	
	public void create(Client client){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("insert into Client(id_client, nom, prenom, no_rue, voie, code_postal, ville, pays) values(?, ?, ?, ?, ?, ?, ?, ?)",
				 Statement.RETURN_GENERATED_KEYS);
				 req.setInt(1,client.getIdclient());req.setString(2,client.getNom());req.setString(3,client.getPrenom());
				 req.setString(4, client.getNorue());req.setString(5,client.getVoie());req.setString(6, client.getPostale());
				 req.setString(7, client.getVille());req.setString(8, client.getPays());
				 int nbLignes = req.executeUpdate();
				 ResultSet res = req.executeQuery();
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb insert" + sqle.getMessage());
		 }
	}
	
	public void update(Client client){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("update Client set nom=?, prenom=?, no_rue=?, voie=?, code_postal=?, ville=?, pays=? where id_client=?");
		 req.setInt(8,client.getIdclient() );req.setString(1, client.getNom());req.setString(2,client.getPrenom());
		 req.setString(3, client.getNorue());req.setString(4, client.getVoie());req.setString(5, client.getPostale());
		 req.setString(6, client.getVille());req.setString(7, client.getPays());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb update" + sqle.getMessage());
		 }
	}
	
	public void delete(Client client){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement requete = laConnexion.prepareStatement("delete from Client where id_client=?");
				 requete.setInt(1, client.getIdclient());
				 int nbLignes = requete.executeUpdate();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb delete" + sqle.getMessage());
		 }
	}
	@Override
	public List<Client> findAll() {
		Connection laConnexion=null;
		 List<Client> list = new ArrayList<Client>();
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Client");
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 list.add(new Client(res.getInt(1),res.getString(2) ,res.getString(3),res.getString(4), res.getString(5),res.getString(6), res.getString(7),res.getString(8)));
				 }
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		return list;
	}
}
