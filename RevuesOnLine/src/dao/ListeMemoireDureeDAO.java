package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metier.*;

public class ListeMemoireDureeDAO implements DureeDAO {

	private static ListeMemoireDureeDAO instance;

	private List<Duree> donnees;


	public static ListeMemoireDureeDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireDureeDAO();
		}

		return instance;
	}

	private ListeMemoireDureeDAO() {

		this.donnees = new ArrayList<Duree>();

		this.donnees.add(new Duree(1, "test"));
		this.donnees.add(new Duree(2, "test"));
	}


	public void create(Duree objet) {

		objet.setIdDuree(3);

		// Ne fonctionne que si l'objet m�tier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setIdDuree(objet.getIdDuree() + 1);
		}
		this.donnees.add(objet);
	}

	public void update(Duree objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	public void delete(Duree objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}
	public Duree GetById(int id) {
		// Ne fonctionne que si l'objet m�tier est bien fait...
				int idx = this.donnees.indexOf(new Duree(id, "test"));
				if (idx == -1) {
					throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
				} else {
					return this.donnees.get(idx);
				}
	}
	
	public List<Duree> findAll() {

		return this.donnees;
	}

}
