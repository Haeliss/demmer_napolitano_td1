package dao;

import java.util.List;

import metier.Duree;

public interface DureeDAO extends DAO<Duree>{
	public abstract Duree GetById(int id);
	public abstract List<Duree> findAll();
}
