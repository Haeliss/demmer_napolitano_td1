package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import metier.*;

public class ListeMemoireAbonnementDAO implements AbonnementDAO{
	
	private static ListeMemoireAbonnementDAO instance;
	private List<DatesAbonnement> donnees=new ArrayList<DatesAbonnement>();


	public static ListeMemoireAbonnementDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireAbonnementDAO();
		}

		return instance;
	}
	
	private ListeMemoireAbonnementDAO() {}

	@Override
	public HashMap<Revue, DatesAbonnement> getAbonnementByClient(Client c) {
		// TODO Auto-generated method stub
		return c.getLesAbos();
	}

	@Override
	public void createAbonnement(Client c, Revue r, String dateDebut, String dateFin, Duree d) {
		c.getLesAbos().put(r, new DatesAbonnement(dateDebut,dateFin,d));
		
	}

	@Override
	public void updateAbonnement(Client c, Revue r, String dateDebut, String datefin, Duree d) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		if (!c.getLesAbos().containsKey(r)) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			c.getLesAbos().put(r, new DatesAbonnement(dateDebut,datefin,d));
		}
		
	}

	@Override
	public void deleteAbonnement(Client c, Revue r) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		if (!c.getLesAbos().containsKey(r)) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			c.getLesAbos().remove(r);
		}
		
	}

	@Override
	public List<DatesAbonnement> findAll() {
		// TODO Auto-generated method stub
		List<Client> client;
		client=DAOFactory.getDAOFactory("listememoire").getClientDAO().findAll();
		for(int i=0; i<client.size(); i++){
			Set<Entry<Revue, DatesAbonnement>> setHm = client.get(i).getLesAbos().entrySet();
		      Iterator<Entry<Revue, DatesAbonnement>> it = setHm.iterator();
		      while(it.hasNext()){
		         Entry<Revue, DatesAbonnement> e = it.next();
		        this.donnees.add(e.getValue());
		      }
		}
		return this.donnees;
	}

}
