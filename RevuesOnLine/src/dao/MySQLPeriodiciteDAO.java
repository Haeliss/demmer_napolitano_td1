package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import metier.*;

public class MySQLPeriodiciteDAO implements PeriodiciteDAO {
	private static MySQLPeriodiciteDAO instance;
	public MySQLPeriodiciteDAO(){}
	
	public static MySQLPeriodiciteDAO getInstance() {
		if (instance==null) {
		instance = new MySQLPeriodiciteDAO();
		}
		return instance;
		}

	public Periodicite GetById(int id){
		 Periodicite period;
		 Connection laConnexion=null;
		 int i = 0;
		 String s=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select id_periodicite, libelle from Periodicite where id_periodicite=?");
				 req.setInt(1,id );
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 i=res.getInt(1);
					 s=res.getString(2);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 period =new Periodicite(i,s);
		 return period;
	}
	
	public void create(Periodicite periode){
		Connection laConnexion=null;
		 try {							//Essaie de se connecter et de faire la requ�te
		 laConnexion = Connexion.creeConnexion();		//Se connecte � la bdd
		 PreparedStatement req = laConnexion.prepareStatement("insert into Periodicite(id_periodicite, libelle) values(?, ?)",
				 Statement.RETURN_GENERATED_KEYS);
		 req.setInt(1,periode.getId());req.setString(2, periode.getLibel());
				 int nbLignes = req.executeUpdate();
				 ResultSet res = req.getGeneratedKeys();
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)	//Ferme la connection
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {		//Affiche l'erreur
		System.out.println("Pb insert" + sqle.getMessage());
		 }
	}
	
	public void update(Periodicite periode){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("update Periodicite set libelle=? where id_periodicite=?");
				 req.setString(1,periode.getLibel());req.setInt(2,periode.getId());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb update" + sqle.getMessage());
		 }
	}
	
	public void delete(Periodicite periode){
		 Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement requete = laConnexion.prepareStatement("delete from Periodicite where id_periodicite=?");
				 requete.setInt(1, periode.getId());
				 int nbLignes = requete.executeUpdate();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb delete" + sqle.getMessage());
		 }
	}

	@Override
	public List<Periodicite> findAll() {
		Connection laConnexion=null;
		 List<Periodicite> list = new ArrayList<Periodicite>();
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Periodicite");
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 list.add(new Periodicite(res.getInt(1), res.getString(2)));
				 }
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		return list;
	}
}
