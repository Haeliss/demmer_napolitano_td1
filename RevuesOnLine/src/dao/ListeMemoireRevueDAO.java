package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metier.*;

public class ListeMemoireRevueDAO implements RevueDAO {

	private static ListeMemoireRevueDAO instance;

	private List<Revue> donnees;


	public static ListeMemoireRevueDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireRevueDAO();
		}

		return instance;
	}

	private ListeMemoireRevueDAO() {

		this.donnees = new ArrayList<Revue>();
		HashMap<Duree,Float>map =new HashMap<Duree,Float>();
		map.put(new Duree(1,"coucou"),1f);
		this.donnees.add(new Revue(1, "Naimpair", "nain qui a un enfant", "Visuel", 5, new Periodicite(1, "Mensuel"), map));
		this.donnees.add(new Revue(2, "Nainrondi", "nain qui a du ventre", "Visuel", 7, new Periodicite(2, "Quotidien"), map));
	}


	@Override
	public void create(Revue objet) {

		objet.setIdrevue(3);

		// Ne fonctionne que si l'objet m�tier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setIdrevue(objet.getIdrevue() + 1);
		}
		this.donnees.add(objet);
	}

	@Override
	public void update(Revue objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Revue objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}
	public Revue GetById(int id) {
		// Ne fonctionne que si l'objet m�tier est bien fait...
				int idx = this.donnees.indexOf(new Revue(id, "test", "test2", "test3", 0, new Periodicite(1, "libelle"), null/*new HashMap<Duree,Float>(new Duree(0, null),0.0)*/));
				if (idx == -1) {
					throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
				} else {
					return this.donnees.get(idx);
				}
	}
	
	public List<Revue> findAll() {

		return this.donnees;
	}

}