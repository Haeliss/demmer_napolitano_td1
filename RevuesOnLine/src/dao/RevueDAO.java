package dao;

import java.util.List;

import metier.Revue;

public interface RevueDAO extends DAO<Revue>{
	public abstract Revue GetById(int id);
	public abstract List<Revue> findAll();
}
