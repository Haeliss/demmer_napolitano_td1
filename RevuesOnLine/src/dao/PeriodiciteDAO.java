package dao;
import java.util.List;

import metier.Periodicite;

public interface PeriodiciteDAO extends DAO<Periodicite>{
	public abstract Periodicite GetById(int id);
	public abstract List<Periodicite> findAll();
}
