package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import metier.*;

public class MySQLDureeDAO implements DureeDAO {
	private static MySQLDureeDAO instance;
	public MySQLDureeDAO(){}
	
	public static MySQLDureeDAO getInstance() {
		if (instance==null) {
		instance = new MySQLDureeDAO();
		}
		return instance;
		}

	public Duree GetById(int id){
		 Duree duree;
		 int i=0;
		 String s = null;
		 Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Duree where id_duree=?");
				 req.setInt(1,id );
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 i=res.getInt(1);
					 s=res.getString(2);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 duree=new Duree(i,s);
		 return duree;
	}
	
	public void create(Duree duree){
		Connection laConnexion=null;
		 try {							//Essaie de se connecter et de faire la requ�te
		 laConnexion = Connexion.creeConnexion();		//Se connecte � la BdD
		 PreparedStatement req = laConnexion.prepareStatement("insert into Duree(id_duree, libelle_formule) values(?, ?)",
				 Statement.RETURN_GENERATED_KEYS);
		 req.setInt(1,duree.getIdDuree());req.setString(2,duree.getLibelleFormule());
				 int nbLignes = req.executeUpdate();
				 ResultSet res = req.getGeneratedKeys();
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)	//Ferme la connexion
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {		//Affiche l'erreur
		System.out.println("Pb insert" + sqle.getMessage());
		 }
	}
	
	public void update(Duree duree){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("update Duree set libelle_formule=? where id_duree=?");
				 req.setString(1,duree.getLibelleFormule());req.setInt(2,duree.getIdDuree());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb update" + sqle.getMessage());
		 }
	}
	
	public void delete(Duree duree){
		 Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement requete = laConnexion.prepareStatement("delete from Duree where id_duree=?");
				 requete.setInt(1, duree.getIdDuree());
				 int nbLignes = requete.executeUpdate();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb delete" + sqle.getMessage());
		 }
	}

	@Override
	public List<Duree> findAll() {
		Connection laConnexion=null;
		 List<Duree> list = new ArrayList<Duree>();
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Periodicite");
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 list.add(new Duree(res.getInt(1), res.getString(2)));
				 }
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		return list;
	}

}
