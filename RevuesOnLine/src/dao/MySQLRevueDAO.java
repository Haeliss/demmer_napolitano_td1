package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metier.*;

public class MySQLRevueDAO implements RevueDAO{
	private static MySQLRevueDAO instance;
	public MySQLRevueDAO(){}
	public static MySQLRevueDAO getInstance() {
		if (instance==null) {
		instance = new MySQLRevueDAO();
		}
		return instance;
		}
	
	public Revue GetById(int id){
		HashMap<Duree,Float> formule= new HashMap<>();
		Revue revue;Periodicite period;Duree duree;
		int idRevue=0,idPeriod=0,idDuree=0;
		float tarif=0;float reduction=0;
		String titre=null, desc=null, visuel=null;
		 Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Revue, Formule where id_revue=?");
				 req.setInt(1,id );
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					idRevue=res.getInt(1);titre=res.getString(2);
					desc=res.getString(3);tarif=res.getFloat(4);
					visuel=res.getString(5);idPeriod=res.getInt(6);
					idDuree=res.getInt(7); reduction=res.getFloat(8);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
	 	System.out.println("Pb select" + sqle.getMessage());
		 }
		 period= MySQLPeriodiciteDAO.getInstance().GetById(idPeriod);
		 duree= MySQLDureeDAO.getInstance().GetById(idDuree);
		 formule.put(duree, reduction);
		 revue=new Revue(idRevue,titre,desc,visuel,tarif,period,formule);
		 return revue;
	}
	public void create(Revue revue){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("insert into Revue(id_revue, titre, description, tarif_numero, visuel, id_periodicite) values(?, ?, ?, ?, ?, ?)",
				 Statement.RETURN_GENERATED_KEYS);
				 req.setInt(1,revue.getIdrevue());req.setString(2, revue.getTitre());req.setString(3, revue.getDesc());
				 req.setDouble(4, revue.getTarif());req.setString(5, revue.getVisuel());req.setInt(6, revue.getPeriod().getId());
				 int nbLignes = req.executeUpdate();
				 ResultSet res = req.getGeneratedKeys();
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb insert" + sqle.getMessage());
		 }
	}
	
	public void update(Revue revue){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("update Revue set titre=?, description=?, tarif_numero=?, visuel=?, id_periodicite=? where id_revue=?");
		 		 req.setString(1, revue.getTitre());req.setString(2, revue.getDesc());req.setDouble(3, revue.getTarif());
		 		 req.setString(4, revue.getVisuel());req.setInt(5, revue.getPeriod().getId());req.setInt(6, revue.getIdrevue());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb update" + sqle.getMessage());
		 }
	}
	
	public void delete(Revue revue){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement requete = laConnexion.prepareStatement("delete from Revue where id_revue=?");
				 requete.setInt(1, revue.getIdrevue());
				 int nbLignes = requete.executeUpdate();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb delete" + sqle.getMessage());
		 }
	}
	@Override
	public List<Revue> findAll() {
		Connection laConnexion=null;
		List<Revue> list = new ArrayList<Revue>();
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Revue");
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 list.add(new Revue(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getFloat(5), MySQLPeriodiciteDAO.getInstance().GetById(res.getInt(6))));
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
	 	System.out.println("Pb select" + sqle.getMessage());
		 }
		return null;
	}
}
