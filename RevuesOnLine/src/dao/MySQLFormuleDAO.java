package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import metier.*;

public class MySQLFormuleDAO implements FormuleDAO{
	private static MySQLFormuleDAO instance;
	public MySQLFormuleDAO(){}
	
	public static MySQLFormuleDAO getInstance() {
		if (instance==null) {
		instance = new MySQLFormuleDAO();
		}
		return instance;
		}
	public HashMap<Duree, Float> getFormulesByRevue(Revue revue) {
		HashMap<Duree, Float> formule = new HashMap<Duree, Float>();
		 Duree duree;
		 int idduree=0;
		 float reduction=0;
		 String s = null;
		 Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Formule where id_revue=?");
				 req.setInt(1,revue.getIdrevue());
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 idduree=res.getInt(2);
					 reduction=res.getFloat(3);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 duree=MySQLDureeDAO.getInstance().GetById(idduree);
		 formule.put(duree, reduction);
		 return formule;
	}
	public void createFormule(Revue revue, Duree duree, float reduction){
		Connection laConnexion=null;
		 try {							//Essaie de se connecter et de faire la requ�te
		 laConnexion = Connexion.creeConnexion();		//Se connecte � la bdd
		 PreparedStatement req = laConnexion.prepareStatement("insert into Formule(id_revue, id_duree, reduction) values(?, ?, ?)",
				 Statement.RETURN_GENERATED_KEYS);
		 req.setInt(1,revue.getIdrevue());req.setInt(2,duree.getIdDuree());req.setFloat(3,reduction);
				 int nbLignes = req.executeUpdate();
				 ResultSet res = req.getGeneratedKeys();
				 if (res.next()) {
				 int cle = res.getInt(1);
				 }
		         if (res != null)	//Ferme la connexion
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {		//Affiche l'erreur
		System.out.println("Pb insert" + sqle.getMessage());
		 }
	}
	
	public void updateFormule(Revue revue, Duree duree, float reduction){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("update Formule set reduction=? where id_revue=? and id_duree=?");
		 req.setFloat(1,reduction);req.setInt(2,revue.getIdrevue());req.setInt(3,duree.getIdDuree());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb update" + sqle.getMessage());
		 }
	}
	
	public void deleteFormule(Revue revue, Duree duree){
		 Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement requete = laConnexion.prepareStatement("delete from Formule where id_revue=? and id_duree=?");
				 requete.setInt(1,revue.getIdrevue());requete.setInt(2,duree.getIdDuree());
				 int nbLignes = requete.executeUpdate();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb delete" + sqle.getMessage());
		 }
	}

	@Override
	public List<Formule> findAll() {
		List<Formule> list = new ArrayList<Formule>();
		Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Formule");
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 list.add(new Formule(MySQLRevueDAO.getInstance().GetById(res.getInt(1)), MySQLDureeDAO.getInstance().GetById(res.getInt(2)), res.getFloat(3)));
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 return list;
	}
}

