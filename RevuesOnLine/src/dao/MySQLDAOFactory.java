package dao;
import dao.*;

public class MySQLDAOFactory extends DAOFactory{

	@Override
	public PeriodiciteDAO getPeriodiciteDAO() {
		// TODO Auto-generated method stub
		return MySQLPeriodiciteDAO.getInstance();
	}

	@Override
	public RevueDAO getRevueDAO() {
		// TODO Auto-generated method stub
		return MySQLRevueDAO.getInstance();
	}

	@Override
	public FormuleDAO getFormuleDAO() {
		// TODO Auto-generated method stub
		return MySQLFormuleDAO.getInstance();
	}

	@Override
	public DureeDAO getDureeDAO() {
		// TODO Auto-generated method stub
		return MySQLDureeDAO.getInstance();
	}

	@Override
	public ClientDAO getClientDAO() {
		// TODO Auto-generated method stub
		return MySQLClientDAO.getInstance();
	}

	@Override
	public AbonnementDAO getAbonnementDAO() {
		// TODO Auto-generated method stub
		return MySQLAbonnementDAO.getInstance();
	}

}
