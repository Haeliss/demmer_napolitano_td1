package dao;

import java.util.HashMap;
import java.util.List;

import metier.*;

public interface FormuleDAO {
	public HashMap<Duree, Float> getFormulesByRevue(Revue revue);
    public void createFormule(Revue r, Duree d, float reduction);
    public void updateFormule(Revue r, Duree d, float reduction);
    public void deleteFormule(Revue r, Duree d);
    public abstract List<Formule> findAll();
}
