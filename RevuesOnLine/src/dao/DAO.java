package dao;

import java.util.List;

import metier.Client;

public interface DAO <T>{
	public abstract void create(T objet);
	public abstract void update(T objet);
	public abstract void delete(T objet);
}
