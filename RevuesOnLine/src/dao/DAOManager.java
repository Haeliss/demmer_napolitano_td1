package dao;

public class DAOManager {
	private static DAOFactory dao;
	public static void setCurrentFactory(String p) {
		dao=DAOFactory.getDAOFactory(p);
	}
	public static DAOFactory getCurrentFactory(){
		return dao;
	}
}
