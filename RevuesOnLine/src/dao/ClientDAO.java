package dao;

import java.util.List;

import metier.Client;

public interface ClientDAO extends DAO<Client>{
	public abstract Client GetById(int id);
	public abstract List<Client> findAll();
}
