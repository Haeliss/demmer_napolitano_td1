package dao;

public abstract class DAOFactory {
	public static  DAOFactory getDAOFactory(String cible) { 
		DAOFactory daoF = null; 
		switch (cible.toLowerCase()) { 
		case "mysql": daoF = new MySQLDAOFactory(); break; 
		case "listememoire": daoF = new ListeMemoireDAOFactory(); break; } 
		return daoF; 
		}
		public abstract PeriodiciteDAO getPeriodiciteDAO();
		public abstract RevueDAO getRevueDAO();
		public abstract FormuleDAO getFormuleDAO();
		public abstract DureeDAO getDureeDAO();
		public abstract ClientDAO getClientDAO();
		public abstract AbonnementDAO getAbonnementDAO();
}
