package dao;

import java.util.HashMap;
import java.util.List;

import metier.Client;
import metier.DatesAbonnement;
import metier.Duree;
import metier.Revue;

public interface AbonnementDAO {
	public HashMap<Revue, DatesAbonnement> getAbonnementByClient(Client client);
    public void createAbonnement(Client c, Revue r, String dateDebut, String dateFin, Duree d);
    public void updateAbonnement(Client c, Revue r, String dateDebut, String dateFin, Duree d);
    public void deleteAbonnement(Client c, Revue r);
    public abstract List<DatesAbonnement> findAll();
}
