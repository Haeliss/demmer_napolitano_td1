package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metier.*;

public class ListeMemoireClientDAO implements ClientDAO{


	private static ListeMemoireClientDAO instance;

	private List<Client> donnees;


	public static ListeMemoireClientDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireClientDAO();
		}

		return instance;
	}

	private ListeMemoireClientDAO() {

		this.donnees = new ArrayList<Client>();
		HashMap<Revue,DatesAbonnement> map=new HashMap<Revue,DatesAbonnement>();
		map.put(new Revue("coucou", "koukou", 0, new Periodicite(1,"Mensuel")),new DatesAbonnement("2013-08-08", "2013-08-08", new Duree("duree")));
		this.donnees.add(new Client(1, "nom", "prenom", "norue", "voie", "postal", "ville", "pays",map ));
		this.donnees.add(new Client(2, "nom", "prenom", "norue", "voie", "postal", "ville", "pays",map ));
	}


	public void create(Client objet) {

		objet.setIdclient(3);

		// Ne fonctionne que si l'objet m�tier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setIdclient(objet.getIdclient() + 1);
		}
		
		this.donnees.add(objet);
	}

	public void update(Client objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	public void delete(Client objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}
	public Client GetById(int id) {
		// Ne fonctionne que si l'objet m�tier est bien fait...
				int idx = this.donnees.indexOf(new Client(id,"nom", "prenom", "norue", "voie", "postal", "ville", "pays", null/*HashMap<Revue,Abonnement>*/));
				if (idx == -1) {
					throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
				} else {
					return this.donnees.get(idx);
				}
	}
	
	public List<Client> findAll() {

		return this.donnees;
	}
}
