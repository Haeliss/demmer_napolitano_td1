package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metier.*;

public class MySQLAbonnementDAO implements AbonnementDAO{
	private static MySQLAbonnementDAO instance;
	public MySQLAbonnementDAO(){}
	public static MySQLAbonnementDAO getInstance() {
		if (instance==null) {
		instance = new MySQLAbonnementDAO();
		}
		return instance;
		}
	
	public HashMap<Revue, DatesAbonnement> getAbonnementByClient(Client client){
		DatesAbonnement abonnement;Revue revue = null; Duree duree;
		String dateDebut = null, dateFin=null;
		int idDuree = 0;
		HashMap<Revue, DatesAbonnement> abos = new HashMap<Revue, DatesAbonnement>();
		 Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Abonnement where id_client=?");
				 req.setInt(1,client.getIdclient() );
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 revue.setIdrevue(res.getInt(2));
					 dateDebut=res.getString(3);
					 dateFin=res.getString(4);
					 idDuree=res.getInt(5);
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 revue=MySQLRevueDAO.getInstance().GetById(revue.getIdrevue());
		 duree=MySQLDureeDAO.getInstance().GetById(idDuree);
		 abonnement=new DatesAbonnement(dateDebut,dateFin,duree);
		 HashMap map= new HashMap<Revue,DatesAbonnement>();
		 map.put(revue, abonnement);
		 return map;
	}
	
	public void createAbonnement(Client c, Revue r, String dateDebut, String dateFin, Duree d){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("insert into Abonnement(id_client, id_revue, date_debut, date_fin, id_duree_choisie) values(?, ?, ?, ?)");
				 req.setInt(1,c.getIdclient());req.setInt(2,r.getIdrevue());
				 req.setString(3, dateDebut);req.setString(4,dateFin);req.setInt(5,d.getIdDuree());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb insert" + sqle.getMessage());
		 }
	}
	
	public void updateAbonnement(Client c, Revue r, String dateDebut, String dateFin, Duree d){
		Connection laConnexion=null; 
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement req = laConnexion.prepareStatement("update Abonnement set date_debut=?, date_fin=?, id_duree_choisie=? where id_client=? and id_revue=?");
		 req.setInt(4,c.getIdclient());req.setInt(5,r.getIdrevue());
		 req.setString(1,dateDebut);req.setString(2,dateFin);req.setInt(2,d.getIdDuree());
				 int nbLignes = req.executeUpdate();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb update" + sqle.getMessage());
		System.out.print("L'op�ration n'a pas �t� ");
		 }
	}
	
	public void deleteAbonnement(Client c, Revue r){
		Connection laConnexion=null;
		 try {
		 laConnexion = Connexion.creeConnexion();
		 PreparedStatement requete = laConnexion.prepareStatement("delete from DatesAbonnement where id_client=? AND id_revue=?");
				 requete.setInt(1,c.getIdclient() );requete.setInt(1,r.getIdrevue() );
				 int nbLignes = requete.executeUpdate();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb delete" + sqle.getMessage());
		 }
	}
	public List<DatesAbonnement> findAll() {
		List<DatesAbonnement> list = new ArrayList<DatesAbonnement>();
		Connection laConnexion=null;
		 try {							
		 laConnexion = Connexion.creeConnexion();		
		 PreparedStatement req = laConnexion.prepareStatement("select * from Abonnement");
				 ResultSet res = req.executeQuery();
				 while(res.next()){
					 list.add(new DatesAbonnement(res.getString(3), res.getString(4),MySQLDureeDAO.getInstance().GetById(res.getInt(5))));
				 }
		         if (res != null)
			     res.close();
			     if (req != null)
			     req.close();
			     if (laConnexion != null)
			     laConnexion.close();
		 } catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
		 return list;
	}
}
