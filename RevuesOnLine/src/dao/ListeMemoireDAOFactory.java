package dao;
import dao.*;

public class ListeMemoireDAOFactory extends DAOFactory{

	@Override
	public PeriodiciteDAO getPeriodiciteDAO() {
		// TODO Auto-generated method stub
		return ListeMemoirePeriodiciteDAO.getInstance();
	}

	@Override
	public RevueDAO getRevueDAO() {
		// TODO Auto-generated method stub
		return ListeMemoireRevueDAO.getInstance();
	}

	@Override
	public FormuleDAO getFormuleDAO() {
		// TODO Auto-generated method stub
		return ListeMemoireFormuleDAO.getInstance();
	}

	@Override
	public DureeDAO getDureeDAO() {
		// TODO Auto-generated method stub
		return ListeMemoireDureeDAO.getInstance();
	}

	@Override
	public ClientDAO getClientDAO() {
		// TODO Auto-generated method stub
		return ListeMemoireClientDAO.getInstance();
	}

	@Override
	public AbonnementDAO getAbonnementDAO() {
		// TODO Auto-generated method stub
		return ListeMemoireAbonnementDAO.getInstance();
	}

}
