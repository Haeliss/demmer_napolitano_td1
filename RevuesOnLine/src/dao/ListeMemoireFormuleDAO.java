package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.List;

import metier.*;

public class ListeMemoireFormuleDAO implements FormuleDAO{
	
	private static ListeMemoireFormuleDAO instance;
	private List<Formule> donnees=new ArrayList<Formule>();

	public static ListeMemoireFormuleDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireFormuleDAO();
		}

		return instance;
	}
	
	private ListeMemoireFormuleDAO() {}

	@Override
	public HashMap<Duree, Float> getFormulesByRevue(Revue r) {
		return r.getFormule();
	}

	@Override
	public void createFormule(Revue r, Duree d, float reduction) {
		r.getFormule().put(d, reduction);
		
	}

	@Override
	public void updateFormule(Revue r, Duree d, float reduction) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		
		if (!r.getFormule().containsKey(d)) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			r.getFormule().put(d, reduction);
		}
		
	}

	@Override
	public void deleteFormule(Revue r, Duree d) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		
		if (!r.getFormule().containsKey(d)) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			r.getFormule().remove(d);
		}
	}

	@Override
	public List<Formule> findAll() {
		// TODO Auto-generated method stub
		List<Revue> revue;
		List<Duree> duree=new ArrayList<Duree>();
		List<Float> reduc=new ArrayList<Float>();
		revue=DAOFactory.getDAOFactory("listememoire").getRevueDAO().findAll();
		for(int i=0; i<revue.size(); i++){
			Set<Entry<Duree, Float>> setHm = revue.get(i).getFormule().entrySet();
		      Iterator<Entry<Duree, Float>> it = setHm.iterator();
		      while(it.hasNext()){
		         Entry<Duree, Float> e = it.next();
		         duree.add(e.getKey());
		         reduc.add(e.getValue());
		      }
		      this.donnees.add(new Formule(revue.get(i),duree.get(i),reduc.get(i)));
		}
		return this.donnees;
	}
	}
	
