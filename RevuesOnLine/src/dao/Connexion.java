package dao;
import java.sql.*;
public class Connexion {
 public static Connection creeConnexion() { //Fonction qui cr�e la connexion avec la base de donn�es
	 String url ="jdbc:mysql://infodb.iutmetz.univ-lorraine.fr:3306/demmer3u_RevuesOnLine";
	 String login = "demmer3u_appli";
	 String pwd = "31604860";
	 Connection maConnexion = null;
	 try {									//Essaie la connection et si �choue renvoie l'erreur.
		 maConnexion = DriverManager.getConnection(url, login, pwd);
	 } catch (SQLException sqle) {
     System.out.println("Erreur connexion" + sqle.getMessage());
	 }
	 return maConnexion;
 	 }
 public static void AjouterPeriodicite(int id, String libel) {	//M�thode qui ajoute une p�riodicit�
	 Connection laConnexion=null;
	 try {							//Essaie de se connecter et de faire la requ�te
	 laConnexion = creeConnexion();		//Se connecte � la BdD
	 PreparedStatement req = laConnexion.prepareStatement("insert into Periodicite(id_periodicite, libelle) values(?, ?)",
			 Statement.RETURN_GENERATED_KEYS);
			 req.setInt(1,id );req.setString(2, libel);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)	//Ferme la connexion
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {		//Affiche l'erreur
	System.out.println("Pb insert" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void SupprimerPeriodicite(int id) { 	//M�thode qui supprime une p�riodicit�
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement requete = laConnexion.prepareStatement("delete from Periodicite where id_periodicite=?");
			 requete.setInt(1, id);
			 int nbLignes = requete.executeUpdate();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb delete" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void ModifierPeriodicite(int id, String libel) { //M�thode qui modifie une p�riodicit�
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("update Periodicite set libelle=? where id_periodicite=?",
	 		 Statement.RETURN_GENERATED_KEYS);
			 req.setString(1,libel );req.setInt(2, id);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb update" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void AjouterRevue(int idRevue, String titre, String desc, double tarif, String visuel,int id ) { //Ajoute une revue
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("insert into Revue(id_revue, titre, description, tarif_numero, visuel, id_periodicite) values(?, ?, ?, ?, ?, ?)",
			 Statement.RETURN_GENERATED_KEYS);
			 req.setInt(1,idRevue );req.setString(2, titre);req.setString(3, desc);
			 req.setDouble(4, tarif);req.setString(5, visuel);req.setInt(6, id);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb insert" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void SupprimerRevue(int idRevue) {
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement requete = laConnexion.prepareStatement("delete from Revue where id_revue=?");
			 requete.setInt(1, idRevue);
			 int nbLignes = requete.executeUpdate();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb delete" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void ModifierRevue(int idRevue, String titre, String desc, double tarif, String visuel,int id) { //M�thode qui modifie une revue
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("update Revue set titre=?, description=?, tarif_numero=?, visuel=?, id_periodicite=? where id_revue=?",
	 		 Statement.RETURN_GENERATED_KEYS);
	 		 req.setString(1, titre);req.setString(2, desc);req.setDouble(3, tarif);
	 		 req.setString(4, visuel);req.setInt(5, id);req.setInt(6, idRevue);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb update" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void AjouterClient(int idClient, String nom, String prenom, String noRue, String voie, String postale, String ville, String pays) { //M�thode qui ajoute un client
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("insert into Client(id_client, nom, prenom, no_rue, voie, code_postal, ville, pays) values(?, ?, ?, ?, ?, ?, ?, ?)",
			 Statement.RETURN_GENERATED_KEYS);
			 req.setInt(1,idClient );req.setString(2, nom);req.setString(3, prenom);
			 req.setString(4, noRue);req.setString(5, voie);req.setString(6, postale);
			 req.setString(7, ville);req.setString(8, pays);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb insert" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void SupprimerClient(int idClient) { //M�thode qui supprime un client
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement requete = laConnexion.prepareStatement("delete from Client where id_client=?");
			 requete.setInt(1, idClient);
			 int nbLignes = requete.executeUpdate();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb delete" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void ModifierClient(int idClient, String nom, String prenom, String noRue, String voie, String postale, String ville, String pays) { //M�thode qui modifie un client
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("update Client set nom=?, prenom=?, no_rue=?, voie=?, code_postal=?, ville=?, pays=? where id_client=?",
	 		 Statement.RETURN_GENERATED_KEYS);
	 req.setInt(8,idClient );req.setString(1, nom);req.setString(2, prenom);
	 req.setString(3, noRue);req.setString(4, voie);req.setString(5, postale);
	 req.setString(6, ville);req.setString(7, pays);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb update" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void AjouterAbonnement(int idClient, int idRevue, String dateDebut, String dateFin) { //M�thode qui ajoute un abonnement
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("insert into Abonnement(id_client, id_revue, date_debut, date_fin) values(?, ?, ?, ?)",
			 Statement.RETURN_GENERATED_KEYS);
			 req.setInt(1,idClient );req.setInt(2, idRevue);
			 req.setString(3, dateDebut);req.setString(4, dateFin);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb insert" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void SupprimerAbonnement(int idClient, int idRevue) { //M�thode qui supprime un abonnement
	 Connection laConnexion=null;
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement requete = laConnexion.prepareStatement("delete from Abonnement where id_client=? AND id_revue=?");
			 requete.setInt(1, idClient);requete.setInt(2, idRevue);
			 int nbLignes = requete.executeUpdate();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb delete" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
 public static void ModifierAbonnement(int idClient, int idRevue,String dateDebut, String dateFin) {
	 Connection laConnexion=null; 
	 try {
	 laConnexion = creeConnexion();
	 PreparedStatement req = laConnexion.prepareStatement("update Abonnement set date_debut=?, date_fin=? where id_client=? and id_revue=?",
	 		 Statement.RETURN_GENERATED_KEYS);
	 req.setInt(3,idClient );req.setInt(4, idRevue);
	 req.setString(1, dateDebut);req.setString(2, dateFin);
			 int nbLignes = req.executeUpdate();
			 ResultSet res = req.getGeneratedKeys();
			 if (res.next()) {
			 int cle = res.getInt(1);
			 }
	         if (res != null)
		     res.close();
		     if (req != null)
		     req.close();
		     if (laConnexion != null)
		     laConnexion.close();
	 } catch (SQLException sqle) {
	System.out.println("Pb update" + sqle.getMessage());
	System.out.print("L'op�ration n'a pas �t� ");
	 }
	}
}