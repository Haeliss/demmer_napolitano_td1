package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import metier.Periodicite;

public class ListeMemoirePeriodiciteDAO implements PeriodiciteDAO {

	private static ListeMemoirePeriodiciteDAO instance;

	private List<Periodicite> donnees;


	public static ListeMemoirePeriodiciteDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoirePeriodiciteDAO();
		}

		return instance;
	}

	private ListeMemoirePeriodiciteDAO() {

		this.donnees = new ArrayList<Periodicite>();

		this.donnees.add(new Periodicite(1, "Mensuel"));
		this.donnees.add(new Periodicite(2, "Quotidien"));
	}


	@Override
	public void create(Periodicite objet) {

		objet.setId(3);

		// Ne fonctionne que si l'objet m�tier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setId(objet.getId() + 1);
		}
		this.donnees.add(objet);
	}

	@Override
	public void update(Periodicite objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Periodicite objet) {

		// Ne fonctionne que si l'objet m�tier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}
	public Periodicite GetById(int id) {
		// Ne fonctionne que si l'objet m�tier est bien fait...
				int idx = this.donnees.indexOf(new Periodicite(id, "test"));
				if (idx == -1) {
					throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
				} else {
					return this.donnees.get(idx);
				}
	}
	
	public List<Periodicite> findAll() {

		return this.donnees;
	}

}