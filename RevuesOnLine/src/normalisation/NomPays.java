package normalisation;

public class NomPays {
	public static String Pays(String pays){
		String rPays;
		try{
			Integer.parseInt(pays);
			rPays="Erreur";
		}
		catch(NumberFormatException f){
		try{
		pays=pays.trim();
		rPays=pays;
		if(	pays.equalsIgnoreCase("letzebuerg")||pays.equalsIgnoreCase("let")||pays.equalsIgnoreCase("etzebuerg")||
			pays.equalsIgnoreCase("letzebuer")||pays.equalsIgnoreCase("luxembourg")||pays.equalsIgnoreCase("lux")||
			pays.equalsIgnoreCase("luxembour")||pays.equalsIgnoreCase("uxembourg")){
			rPays="Luxembourg";
		}
		else if(pays.equalsIgnoreCase("belgium")||pays.equalsIgnoreCase("bel")||pays.equalsIgnoreCase("elgium")||
				pays.equalsIgnoreCase("belgiu")||pays.equalsIgnoreCase("belgique")||pays.equalsIgnoreCase("elgique")||
				pays.equalsIgnoreCase("belgiqu")){
			rPays="Belgique";
		}
		else if(pays.equalsIgnoreCase("schweiz")||pays.equalsIgnoreCase("sch")||pays.equalsIgnoreCase("chweiz")||
				pays.equalsIgnoreCase("schwei")||pays.equalsIgnoreCase("switzerland")||pays.equalsIgnoreCase("swi")||
				pays.equalsIgnoreCase("switzerlan")||pays.equalsIgnoreCase("witzerland")||pays.equalsIgnoreCase("suisse")||
				pays.equalsIgnoreCase("sui")||pays.equalsIgnoreCase("uisse")||pays.equalsIgnoreCase("suiss")){
			rPays="Suisse";
		}
		else if(pays==""||pays.length()==0){
			rPays="Erreur";
		}
		}
		catch(NullPointerException e){
				rPays="Erreur";
			}}
		return rPays;

	}
}
