package normalisation;


public class NomVille {
	public static String Ville(String ville){
		String rVille;
		try{
			Integer.parseInt(ville);
			rVille="Erreur";
		}
		catch(NumberFormatException f){
		try{
		ville=ville.trim();
		rVille=ville;
		if(rVille==""||rVille.length()==0){
			rVille="Erreur";
		}
		char [] charTable = rVille.toCharArray();
		charTable[0] = Character.toUpperCase(charTable[0]);
		for(int i=0;i < charTable.length; i++) 
		{  
		if(charTable[i]==' '){
			charTable[i+1]=Character.toUpperCase(charTable[i+1]);
		}
		}
		rVille = new String(charTable);
		rVille=rVille.replace(" L�s ", "-l�s-");rVille=rVille.replace(" L�s-", "-l�s-");rVille=rVille.replace("-l�s ", "-l�s-");
		rVille=rVille.replace(" Le ", "-le-");rVille=rVille.replace(" Le-", "-le-");rVille=rVille.replace("-Le ", "-le-");
		rVille=rVille.replace(" Sous ", "-sous-");rVille=rVille.replace(" Sous-", "-sous-");rVille=rVille.replace("-Sous ", "-sous-");
		rVille=rVille.replace(" Sur ", "-sur-");rVille=rVille.replace(" Sur-", "-sur-");rVille=rVille.replace("-Sur ", "-sur-");
		rVille=rVille.replace(" � ", "-�-");rVille=rVille.replace(" �-", "-�-");rVille=rVille.replace("-� ", "-�-");
		rVille=rVille.replace(" Aux ", "-aux-");rVille=rVille.replace(" Aux-", "-aux-");rVille=rVille.replace("-Aux ", "-aux-");
		if(rVille.contains("Ste ")||rVille.contains("ste ")||rVille.contains("STE ")||
		   rVille.contains("Ste-")||rVille.contains("ste-")||rVille.contains("STE-")){
			rVille=rVille.replace("Ste-", "Sainte-");
			rVille=rVille.replace("ste-", "Sainte-");
			rVille=rVille.replace("STE-", "Sainte-");
			rVille=rVille.replace("Ste ", "Sainte-");
			rVille=rVille.replace("ste ", "Sainte-");
			rVille=rVille.replace("STE ", "Sainte-");
		}
		else if(rVille.contains("St ")||rVille.contains("st ")||rVille.contains("ST ")||
				rVille.contains("St-")||rVille.contains("st-")||rVille.contains("ST-")){
			rVille=rVille.replace("St-", "Saint-");
			rVille=rVille.replace("st-", "Saint-");
			rVille=rVille.replace("ST-", "Saint-");
			rVille=rVille.replace("St ", "Saint-");
			rVille=rVille.replace("st ", "Saint-");
			rVille=rVille.replace("ST ", "Saint-");
		}
		//System.out.println(rVille); Affiche l'�criture des villes pour chaque test
		rVille.replace(' ', '-');
		}
		catch (NullPointerException e){
			rVille="Erreur";
		}}
		return rVille;
	}
}
