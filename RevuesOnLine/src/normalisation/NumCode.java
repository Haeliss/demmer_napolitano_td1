package normalisation;

public class NumCode {
	public static String Code(String code, String pays){
		String rCode;
		try{
		code=code.trim();
		rCode=code;

		if((rCode=="")||(rCode.length()==0)){
			rCode="Erreur";
		}
		if(rCode.length()>=5){
			if(rCode.charAt(0)=='F'){
				if(rCode.charAt(1)=='-')rCode=rCode.substring(2);
				else rCode=rCode.substring(1);
			}
		}
		if(rCode.length()==4){
			if(pays!="Luxembourg"){
				rCode="0"+rCode;
			}
		}
		else if((rCode.charAt(0)=='L')||(rCode.charAt(0)=='B')||(rCode.charAt(0)=='S')){
			if(rCode.charAt(1)=='-')rCode=rCode.substring(2);
			else rCode=rCode.substring(1);
		}
		}
		catch (NullPointerException e){
			rCode="Erreur";
		}
		//System.out.println(rCode); Affiche le code pour chaque test
		return rCode;
	}
}



