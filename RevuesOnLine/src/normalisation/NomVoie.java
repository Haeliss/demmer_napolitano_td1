package normalisation;

public class NomVoie {
	public static String Voie(String voie){
		String rVoie;
		try{
		voie=voie.trim();
		voie=voie.toLowerCase();
		rVoie=voie;
		//System.out.println(rVoie);
		if(rVoie==""||rVoie.length()==0){
			rVoie="Erreur";
		}
		else if(	rVoie.contains(" boul. ")||rVoie.contains(" bd ")||rVoie.contains(" boul ")||
			rVoie.contains(" oulevard ")||rVoie.contains(" boulevar ")||rVoie.contains(" bd. ")||
			rVoie.contains(" bou ")||rVoie.contains(" bou. ")){
			rVoie=rVoie.replace(" boul. ", " boulevard ");rVoie=rVoie.replace(" bd ", " boulevard ");
			rVoie=rVoie.replace(" boul ", " boulevard ");rVoie=rVoie.replace(" oulevard ", " boulevard ");
			rVoie=rVoie.replace(" boulevar ", " boulevard ");rVoie=rVoie.replace(" bd. ", " boulevard ");
			rVoie=rVoie.replace(" bou ", " boulevard ");rVoie=rVoie.replace(" bou. ", " boulevard ");
		}
		else if(rVoie.contains(" av ")||rVoie.contains(" av. ")||rVoie.contains(" venue ")||
				rVoie.contains(" avenu ")||rVoie.contains(" aven ")||rVoie.contains(" aven. ")){
			rVoie=rVoie.replace(" av ", " avenue ");rVoie=rVoie.replace(" av. ", " avenue ");
			rVoie=rVoie.replace(" venue ", " avenue ");rVoie=rVoie.replace(" avenu ", " avenue ");
			rVoie=rVoie.replace(" aven ", " avenue ");rVoie=rVoie.replace(" aven. ", " avenue ");
		}
		else if(rVoie.contains(" faub ")||rVoie.contains(" faub. ")||rVoie.contains(" fg ")||
				rVoie.contains(" aubourg ")||rVoie.contains(" faubour ")||rVoie.contains(" fg. ")||
				rVoie.contains(" fau ")||rVoie.contains(" fau. ")){
			rVoie=rVoie.replace(" faub ", " faubourg ");rVoie=rVoie.replace(" faub. ", " faubourg ");
			rVoie=rVoie.replace(" fg ", " faubourg ");rVoie=rVoie.replace(" aubourg ", " faubourg ");
			rVoie=rVoie.replace(" faubour ", " faubourg ");rVoie=rVoie.replace(" fg. ", " faubourg ");
			rVoie=rVoie.replace(" fau ", " faubourg ");rVoie=rVoie.replace(" fau. ", " faubourg ");
		}
		else if(rVoie.contains(" pl ")||rVoie.contains(" pl. ")||rVoie.contains(" lace ")||
				rVoie.contains(" plac ")||rVoie.contains(" pla. ")||rVoie.contains(" pla ")||
				rVoie.contains(" plc ")||rVoie.contains(" plc. ")){
			rVoie=rVoie.replace(" pl ", " place ");rVoie=rVoie.replace(" pl. ", " place ");
			rVoie=rVoie.replace(" lace ", " place ");rVoie=rVoie.replace(" plac ", " place ");
			rVoie=rVoie.replace(" pla. ", " place ");rVoie=rVoie.replace(" pla ", " place ");
			rVoie=rVoie.replace(" plc ", " place ");rVoie=rVoie.replace(" plc. ", " place ");
		}
		}
		catch(NullPointerException e){
				rVoie="Erreur";
			}
		//System.out.println(rVoie);
		return rVoie;
	}
}






