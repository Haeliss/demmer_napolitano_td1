package normalisation;

public class NumVoie {
	public static String Voie(String nVoie){
		String rnVoie;
		try{
		nVoie=nVoie.trim();
		rnVoie=nVoie;
		if(rnVoie.endsWith(",")){
			rnVoie = rnVoie.substring(0,rnVoie.length()-1);
		}
		try{
			int test = Integer.parseInt (rnVoie);
			return rnVoie+",";		
		}
        catch (NumberFormatException e) {
        	if(rnVoie.endsWith("bis")||rnVoie.endsWith("ter")){
        		return rnVoie+",";	
        	}
        	else 
        	return "Erreur, mauvais format";
        }}
		catch(NullPointerException e){
			return "Erreur";
		}
	}
}

