package test;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.*;

public class TestNumVoie {

//======================= Bien ===========================
	
		@Test
		public void testNumVoie() {
			assertEquals("6,", NumVoie.Voie(" 6 "));
		}
		
		@Test
		public void testNumVoie2() {
			assertEquals("1,", NumVoie.Voie("1,"));
		}
		
		@Test
		public void testNumVoie3() {
			assertEquals("69 bis,", NumVoie.Voie("69 bis"));
		}
		
		@Test
		public void testNumVoie4() {
			assertEquals("1337 ter,", NumVoie.Voie("1337 ter"));
		}
		
		@Test
		public void testNumVoie5() {
			assertEquals("-33554432,", NumVoie.Voie("-33554432"));
		}
		
		
//======================== Pas bien ===============================		
		
		@Test
		public void testNumErreur() {
			assertEquals("Erreur, mauvais format", NumVoie.Voie("a"));
		}
		
		@Test
		public void testNumErreur2() {
			assertEquals("Erreur, mauvais format", NumVoie.Voie(" "));
		}
		
		@Test
		public void testNumErreur3() {
			assertEquals("Erreur, mauvais format", NumVoie.Voie("-"));
		}
		
		@Test
		public void testNumErreur4() {
			assertEquals("Erreur, mauvais format", NumVoie.Voie("1 3 5 7 9"));
		}
		
		@Test
		public void testNumErreur5() {
			assertEquals("Erreur, mauvais format", NumVoie.Voie("2.718281828"));
		}
				
		@Test
		public void testNumErreur6() {
			assertEquals("Erreur, mauvais format", NumVoie.Voie("PoneyVirgule,"));
		}
		
		@Test
		public void testNumErreur7() {
			assertEquals("Erreur", NumVoie.Voie(null));
		}
}

