 package test;

import static org.junit.Assert.*;
import normalisation.*;

import org.junit.Test;

public class TestNomPays {

//======================= Belgique ===========================
	@Test
	public void testbelgium() {
		assertEquals("Belgique", NomPays.Pays(" belgium"));
	}

	@Test
	public void testBelgium() {
		assertEquals("Belgique", NomPays.Pays("Belgium "));
	}
	
	@Test
	public void testbelgique() {
		assertEquals("Belgique", NomPays.Pays("belgique"));
	}
	
	@Test
	public void testBelgiqu() {
		assertEquals("Belgique", NomPays.Pays("Belgiqu"));
	}
	
	@Test
	public void testbelgiqu() {
		assertEquals("Belgique", NomPays.Pays("belgiqu"));
	}
	
	@Test
	public void testBelgique() {
		assertEquals("Belgique", NomPays.Pays("Belgique"));
	}
	
	@Test
	public void testBELGIQUE() {
		assertEquals("Belgique", NomPays.Pays("BELGIQUE"));
	}
	
	@Test
	public void testbELGIQUE() {
		assertEquals("Belgique", NomPays.Pays("bELGIQUE"));
	}
	
	@Test
	public void testBELGIUM() {
		assertEquals("Belgique", NomPays.Pays("BELGIUM"));
	}
	
	@Test
	public void testbELGIUM() {
		assertEquals("Belgique", NomPays.Pays("bELGIUM"));
	}
		
	@Test
	public void testBELGIU() {
		assertEquals("Belgique", NomPays.Pays("BELGIU"));
	}
	
	@Test
	public void testbELGIU() {
		assertEquals("Belgique", NomPays.Pays("bELGIU"));
	}
	
	@Test
	public void testbelgiu() {
		assertEquals("Belgique", NomPays.Pays("belgiu"));
	}
	
	@Test
	public void testBelgiu() {
		assertEquals("Belgique", NomPays.Pays("Belgiu"));
	}
	
	@Test
	public void testELGIUM() {
		assertEquals("Belgique", NomPays.Pays("ELGIUM"));
	}
	
	@Test
	public void testeLGIUM() {
		assertEquals("Belgique", NomPays.Pays("eLGIUM"));
	}
	
	@Test
	public void testelgium() {
		assertEquals("Belgique", NomPays.Pays("elgium"));
	}
	
	@Test
	public void testElgium() {
		assertEquals("Belgique", NomPays.Pays("Elgium"));
	}
//======================== Suisse =================================
		
	@Test
	public void testswitzerland() {
		assertEquals("Suisse", NomPays.Pays("switzerland"));
	}

	@Test
	public void testSwitzerland() {
		assertEquals("Suisse", NomPays.Pays("Switzerland"));
	}
	
	@Test
	public void testsuisse() {
		assertEquals("Suisse", NomPays.Pays("suisse"));
	}
	
	@Test
	public void testSuiss() {
		assertEquals("Suisse", NomPays.Pays("Suiss"));
	}
	
	@Test
	public void testsuiss() {
		assertEquals("Suisse", NomPays.Pays("suiss"));
	}
	
	@Test
	public void testSuisse() {
		assertEquals("Suisse", NomPays.Pays("Suisse"));
	}
	
	@Test
	public void testschweiz() {
		assertEquals("Suisse", NomPays.Pays("schweiz"));
	}
	
	@Test
	public void testSchweiz() {
		assertEquals("Suisse", NomPays.Pays("Schweiz"));
	}
	
	@Test
	public void testsCHWEIZ() {
		assertEquals("Suisse", NomPays.Pays("sCHWEIZ"));
	}
	
	@Test
	public void testSCHWEIZ() {
		assertEquals("Suisse", NomPays.Pays("SCHWEIZ"));
	}
	
	@Test
	public void testSUISSE() {
		assertEquals("Suisse", NomPays.Pays("SUISSE"));
	}
	
	@Test
	public void testsUISSE() {
		assertEquals("Suisse", NomPays.Pays("sUISSE"));
	}
	
	@Test
	public void testuisse() {
		assertEquals("Suisse", NomPays.Pays("uisse"));
	}
	
	@Test
	public void testUisse() {
		assertEquals("Suisse", NomPays.Pays("Uisse"));
	}
	
	@Test
	public void testUISSE() {
		assertEquals("Suisse", NomPays.Pays("UISSE"));
	}
	
	@Test
	public void testuISSE() {
		assertEquals("Suisse", NomPays.Pays("uISSE"));
	}
	
	@Test
	public void testSUISS() {
		assertEquals("Suisse", NomPays.Pays("SUISS"));
	}
	
	@Test
	public void testsUISS() {
		assertEquals("Suisse", NomPays.Pays("sUISS"));
	}
	
	@Test
	public void testSwitzerlan() {
		assertEquals("Suisse", NomPays.Pays("Switzerlan"));
	}
	
	@Test
	public void testswitzerlan() {
		assertEquals("Suisse", NomPays.Pays("switzerlan"));
	}
	
	@Test
	public void testSWITZERLAN() {
		assertEquals("Suisse", NomPays.Pays("SWITZERLAN"));
	}
	
	@Test
	public void testsWITZERLAN() {
		assertEquals("Suisse", NomPays.Pays("sWITZERLAN"));
	}
	
	@Test
	public void testWitzerland() {
		assertEquals("Suisse", NomPays.Pays("Witzerland"));
	}
	
	@Test
	public void testwitzerland() {
		assertEquals("Suisse", NomPays.Pays("witzerland"));
	}
	
	@Test
	public void testWITZERLAND() {
		assertEquals("Suisse", NomPays.Pays("WITZERLAND"));
	}
	
	@Test
	public void testwITZERLAND() {
		assertEquals("Suisse", NomPays.Pays("wITZERLAND"));
	}
	
	@Test
	public void testschwei() {
		assertEquals("Suisse", NomPays.Pays("schwei"));
	}
	
	@Test
	public void testSchwei() {
		assertEquals("Suisse", NomPays.Pays("Schwei"));
	}
	
	@Test
	public void testsCHWEI() {
		assertEquals("Suisse", NomPays.Pays("sCHWEI"));
	}
	
	@Test
	public void testSCHWEI() {
		assertEquals("Suisse", NomPays.Pays("SCHWEI"));
	}
	
	@Test
	public void testChweiz() {
		assertEquals("Suisse", NomPays.Pays("Chweiz"));
	}
	
	@Test
	public void testchweiz() {
		assertEquals("Suisse", NomPays.Pays("chweiz"));
	}
	
	@Test
	public void testcHWEIZ() {
		assertEquals("Suisse", NomPays.Pays("cHWEIZ"));
	}
	
	@Test
	public void testCHWEIZ() {
		assertEquals("Suisse", NomPays.Pays("CHWEIZ"));
	}
//=================== Luxembourg ===========================
	@Test
	public void testluxembourg() {
		assertEquals("Luxembourg", NomPays.Pays("luxembourg"));
	}

	@Test
	public void testLuxembourg() {
		assertEquals("Luxembourg", NomPays.Pays("Luxembourg"));
	}
	
	@Test
	public void testletzebuerg() {
		assertEquals("Luxembourg", NomPays.Pays("letzebuerg"));
	}
	
	@Test
	public void testLetzebuerg() {
		assertEquals("Luxembourg", NomPays.Pays("Letzebuerg"));
	}
	
	@Test
	public void testlux() {
		assertEquals("Luxembourg", NomPays.Pays("lux"));
	}
	
	@Test
	public void testLux() {
		assertEquals("Luxembourg", NomPays.Pays("Lux"));
	}
	
	@Test
	public void testLuxembour() {
		assertEquals("Luxembourg", NomPays.Pays("Luxembour"));
	}
	
	@Test
	public void testluxembour() {
		assertEquals("Luxembourg", NomPays.Pays("luxembour"));
	}
	
	@Test
	public void testLUXEMBOURG() {
		assertEquals("Luxembourg", NomPays.Pays("LUXEMBOURG"));
	}
	
	@Test
	public void testlUXEMBOURG() {
		assertEquals("Luxembourg", NomPays.Pays("lUXEMBOURG"));
	}
	
	@Test
	public void testLETZBUER() {
		assertEquals("Luxembourg", NomPays.Pays("LETZEBUER"));
	}
	
	@Test
	public void testlETZEBUER() {
		assertEquals("Luxembourg", NomPays.Pays("lETZEBUER"));
	}
	
	@Test
	public void testETZBUERG() {
		assertEquals("Luxembourg", NomPays.Pays("ETZEBUERG"));
	}
	
	@Test
	public void testetzebuerg() {
		assertEquals("Luxembourg", NomPays.Pays("etzebuerg"));
	}
	
	@Test
	public void testeTZBUERG() {
		assertEquals("Luxembourg", NomPays.Pays("eTZEBUERG"));
	}
	
	@Test
	public void testEtzebuerg() {
		assertEquals("Luxembourg", NomPays.Pays("Etzebuerg"));
	}
	
	@Test
	public void testlet() {
		assertEquals("Luxembourg", NomPays.Pays("let"));
	}
	
	@Test
	public void testLET() {
		assertEquals("Luxembourg", NomPays.Pays("LET"));
	}
	
	@Test
	public void testLet() {
		assertEquals("Luxembourg", NomPays.Pays("Let"));
	}
	
	@Test
	public void testlET() {
		assertEquals("Luxembourg", NomPays.Pays("lET"));
	}
		
//========================== Autres =====================
@Test
public void testChiffre() {
	assertEquals("Erreur", NomPays.Pays("12345"));
}				

@Test
public void testNull() {
	assertEquals("Erreur", NomPays.Pays(null));
}

@Test
public void testVide() {
	assertEquals("Erreur", NomPays.Pays(" "));
}

}


