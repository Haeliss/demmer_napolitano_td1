package test;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.*;

public class TestNomVille {

//======================= Pr�position ===========================
	
		@Test
		public void testPrep1() {
			assertEquals("Montigny-l�s-Metz", NomVille.Ville(" Montigny l�s Metz"));
		}
		
		@Test
		public void testPrep2() {
			assertEquals("Montigny-l�s-Metz", NomVille.Ville(" Montigny l�s-Metz"));
		}
		
		@Test
		public void testPrep3() {
			assertEquals("Montigny Les Metz", NomVille.Ville(" Montigny les Metz"));
		}
		
		@Test
		public void testPrep4() {
			assertEquals("L�s-Monts-Igniz-De-Messe", NomVille.Ville("L�s-Monts-Igniz-De-Messe"));
		}
		
		@Test
		public void testPrep5() {
			assertEquals("Laipimantai L�sarico", NomVille.Ville("Laipimantai L�sarico"));
		}
		
		@Test
		public void testPrep6() {
			assertEquals("Montigny-l�s-Metz", NomVille.Ville(" Montigny l�s Metz"));
		}
		
		@Test
		public void testPrep7() {
			assertEquals("Montigny-le-Metz", NomVille.Ville(" Montigny le Metz"));
		}
		
		@Test
		public void testPrep8() {
			assertEquals("Montigny-�-Metz", NomVille.Ville(" Montigny � Metz"));
		}
		
		@Test
		public void testPrep9() {
			assertEquals("Chanterelle-sous-Bois", NomVille.Ville(" Chanterelle-sous-Bois"));
		}
		
		@Test
		public void testPrep10() {
			assertEquals("Gif-sur-Yvette", NomVille.Ville("gif sur Yvette"));
		}
		


		
//======================= St/Ste ===============================

		@Test
		public void testSt() {
			assertEquals("Saint-Remy", NomVille.Ville("St-Remy"));
		}
		
		@Test
		public void testSt2() {
			assertEquals("Sainte-Marie", NomVille.Ville("Ste Marie"));
		}
		
		@Test
		public void testSt3() {
			assertEquals("Stefenschon", NomVille.Ville("Stefenschon"));
		}	
		
		@Test
		public void testSt4() {
			assertEquals("Saints Dieux", NomVille.Ville("Saints Dieux"));
		}

		//======================= Autres ============================
				@Test
				public void testChiffre() {
					assertEquals("Erreur", NomVille.Ville("12345"));
				}
				
				@Test
				public void testMaj() {
					assertEquals("Metz", NomVille.Ville("metz"));
				}
				
				@Test
				public void testNull() {
					assertEquals("Erreur", NomVille.Ville(null));
				}
				
				@Test
				public void testVide() {
					assertEquals("Erreur", NomVille.Ville(" "));
				}




}


