package test;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.NumCode;

public class TestNumCode {

	@Test
	public void test57550() {
		assertEquals("57550",NumCode.Code("57550","France"));
	}
	@Test
	public void test7550() {
		assertEquals("07550",NumCode.Code("7550","France"));
	}
	@Test
	public void test75502() {
		assertEquals("7550",NumCode.Code("7550","Luxembourg"));
	}
	@Test
	public void testF57550() {
		assertEquals("57550",NumCode.Code("F57550","France"));
	}
	@Test
	public void testFt7550() {
		assertEquals("07550",NumCode.Code("F-7550","France"));
	}
	@Test
	public void testF7550() {
		assertEquals("07550",NumCode.Code("F7550","France"));
	}
	@Test
	public void testL5750() {
		assertEquals("7550",NumCode.Code("L7550","Luxembourg"));
	}
	@Test
	public void testLt7550() {
		assertEquals("7550",NumCode.Code("L-7550","Luxembourg"));
	}
	@Test
	public void testS5550() {
		assertEquals("5550",NumCode.Code("S5550","Suisse"));
	}
	@Test
	public void testSt5550() {
		assertEquals("5550",NumCode.Code("S-5550","Suisse"));
	}
	@Test
	public void testBt5550() {
		assertEquals("5550",NumCode.Code("B-5550","Belgique"));
	}
	@Test
	public void testB5550() {
		assertEquals("5550",NumCode.Code("B5550","Belgique"));
	}

	//========================== Autres =====================				

		@Test
		public void testNull() {
			assertEquals("Erreur",NumCode.Code(null,null));
		}

		@Test
		public void testVide() {
			assertEquals("Erreur", NumCode.Code(" "," "));
		


}}