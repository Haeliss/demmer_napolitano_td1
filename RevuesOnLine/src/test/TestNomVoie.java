package test;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.*;

public class TestNomVoie {

	@Test
	public void testnull() {
		assertEquals("Erreur", NomVoie.Voie(null));
	}

	@Test
	public void testvide() {
		assertEquals("Erreur", NomVoie.Voie(" "));
	}

//======================= Boulevard ===========================
		@Test
		public void testboulevard() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie(" 3, boulevard des alouettes"));
		}
		
		@Test
		public void testBOULEVARD() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BOULEVARD des alouettes"));
		}
		
		@Test
		public void testBoulevard() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Boulevard des alouettes"));
		}
		
		@Test
		public void testbOULEVARD() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bOULEVARD des alouettes"));
		}
		
		@Test
		public void testOULEVARD() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, OULEVARD des alouettes"));
		}
		
		@Test
		public void testoulevard() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, oulevard des alouettes"));
		}
		
		@Test
		public void testoULEVARD() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, oULEVARD des alouettes"));
		}
		
		@Test
		public void testOulevard() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Oulevard des alouettes"));
		}
		
		@Test
		public void testbOULEVAR() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bOULEVAR des alouettes"));
		}
		
		@Test
		public void testboulevar() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, boulevar des alouettes"));
		}
		
		@Test
		public void testBOULEVAR() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BOULEVAR des alouettes"));
		}
		
		@Test
		public void testBoulevar() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Boulevar des alouettes"));
		}
			
		@Test
		public void testboul() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, boul des alouettes"));
		}
		
		@Test
		public void testBOUL() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BOUL des alouettes"));
		}
		
		@Test
		public void testBoul() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Boul des alouettes"));
		}
		
		@Test
		public void testbOUL() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bOUL des alouettes"));
		}
		
		public void testboul2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, boul. des alouettes"));
		}
		
		@Test
		public void testBOUL2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BOUL. des alouettes"));
		}
		
		@Test
		public void testBoul2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Boul. des alouettes"));
		}
		
		@Test
		public void testbOUL2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bOUL. des alouettes"));
		}
		
		@Test
		public void testbou() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bou des alouettes"));
		}
		
		@Test
		public void testBOU() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BOU des alouettes"));
		}
		
		@Test
		public void testBou() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Bou des alouettes"));
		}
		
		@Test
		public void testbOU() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bOU des alouettes"));
		}
		
		@Test
		public void testbou2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bou. des alouettes"));
		}
		
		@Test
		public void testBOU2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BOU. des alouettes"));
		}
		
		@Test
		public void testBou2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Bou. des alouettes"));
		}
		
		@Test
		public void testbOU2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bOU. des alouettes"));
		}
		@Test
		public void testbD() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bD des alouettes"));
		}
		
		@Test
		public void testBD() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BD des alouettes"));
		}
		
		@Test
		public void testBd() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Bd des alouettes"));
		}
		
		@Test
		public void testbd() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bd des alouettes"));
		}
		
		@Test
		public void testbD2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bD. des alouettes"));
		}
		
		@Test
		public void testBD2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, BD. des alouettes"));
		}
		
		@Test
		public void testBd2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, Bd. des alouettes"));
		}
		
		@Test
		public void testbd2() {
			assertEquals("3, boulevard des alouettes", NomVoie.Voie("3, bd. des alouettes"));
		}
		
//============================= Avenue ========================== 	
		
		@Test
		public void testavenue() {
			assertEquals("5, avenue des lilas", NomVoie.Voie(" 5, avenue des lilas"));
		}
		
		@Test
		public void testAVENUE() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, AVENUE des lilas"));
		}
		
		@Test
		public void testAvenue() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Avenue des lilas"));
		}
		
		@Test
		public void testaVENUE() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aVENUE des lilas"));
		}
		
		@Test
		public void testvenue() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, venue des lilas"));
		}
		
		@Test
		public void testVENUE() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, VENUE des lilas"));
		}
		
		@Test
		public void testVenue() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Venue des lilas"));
		}
		
		@Test
		public void testvENUE() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, vENUE des lilas"));
		}
		
		@Test
		public void testavenu() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, avenu des lilas"));
		}
		
		@Test
		public void testAVENU() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, AVENU des lilas"));
		}
		
		@Test
		public void testAvenu() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Avenu des lilas"));
		}
		
		@Test
		public void testaVENU() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aVENU des lilas"));
		}
		
		@Test
		public void testaven() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aven des lilas"));
		}
		
		@Test
		public void testAVEN() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, AVEN des lilas"));
		}
		
		@Test
		public void testAven() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Aven des lilas"));
		}
		
		@Test
		public void testaVEN() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aVEN des lilas"));
		}
		
		@Test
		public void testaven2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aven. des lilas"));
		}
		
		@Test
		public void testAVEN2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, AVEN. des lilas"));
		}
		
		@Test
		public void testAven2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Aven. des lilas"));
		}
		
		@Test
		public void testaVEN2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aVEN. des lilas"));
		}
		
		@Test
		public void testav() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, av des lilas"));
		}
		
		@Test
		public void testAV() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, AV des lilas"));
		}
		
		@Test
		public void testAv() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Av des lilas"));
		}
		
		@Test
		public void testaV() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aV des lilas"));
		}
		
		@Test
		public void testav2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, av. des lilas"));
		}
		
		@Test
		public void testAV2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, AV. des lilas"));
		}
		
		@Test
		public void testAv2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, Av. des lilas"));
		}
		
		@Test
		public void testaV2() {
			assertEquals("5, avenue des lilas", NomVoie.Voie("5, aV. des lilas"));
		}
		
//============================= Faubourg ================================
		
		@Test
		public void testfaubourg() {
			assertEquals("1, faubourg du pre", NomVoie.Voie(" 1, faubourg du pre"));
		}
		
		@Test
		public void testFAUBOURG() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FAUBOURG du pre"));
		}
		
		@Test
		public void testFaubourg() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Faubourg du pre"));
		}
		
		@Test
		public void testfAUBOURG() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fAUBOURG du pre"));
		}
		
		@Test
		public void testfaubour() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, faubour du pre"));
		}
		
		@Test
		public void testFAUBOUR() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FAUBOUR du pre"));
		}
		
		@Test
		public void testFaubour() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Faubour du pre"));
		}
		
		@Test
		public void testfAUBOUR() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fAUBOUR du pre"));
		}
		
		@Test
		public void testaubourg() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, aubourg du pre"));
		}
		
		@Test
		public void testAUBOURG() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, AUBOURG du pre"));
		}
		
		@Test
		public void testAubourg() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Aubourg du pre"));
		}
		
		@Test
		public void testaUBOURG() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, aUBOURG du pre"));
		}
		
		@Test
		public void testfaub() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, faub du pre"));
		}
		
		@Test
		public void testFAUB() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FAUB du pre"));
		}
		
		@Test
		public void testFaub() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Faub du pre"));
		}
		
		@Test
		public void testfAUB() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fAUB du pre"));
		}
		
		@Test
		public void testfaub2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, faub. du pre"));
		}
		
		@Test
		public void testFAUB2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FAUB. du pre"));
		}
		
		@Test
		public void testFaub2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Faub. du pre"));
		}
		
		@Test
		public void testfAUB2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fAUB. du pre"));
		}
		
		@Test
		public void testfau() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fau du pre"));
		}
		
		@Test
		public void testFAU() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FAU du pre"));
		}
		
		@Test
		public void testFau() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Fau du pre"));
		}
		
		@Test
		public void testfAU() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fAU du pre"));
		}
		
		@Test
		public void testfau2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fau. du pre"));
		}
		
		@Test
		public void testFAU2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FAU. du pre"));
		}
		
		@Test
		public void testFau2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Fau. du pre"));
		}
		
		@Test
		public void testfAU2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fAU. du pre"));
		}
		
		@Test
		public void testfg() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fg du pre"));
		}
		
		@Test
		public void testFG() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FG du pre"));
		}
		
		@Test
		public void testFg() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Fg du pre"));
		}
		
		@Test
		public void testfG() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fG du pre"));
		}
		
		@Test
		public void testfg2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fg. du pre"));
		}
		
		@Test
		public void testFG2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, FG. du pre"));
		}
		
		@Test
		public void testFg2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, Fg. du pre"));
		}
		
		@Test
		public void testfG2() {
			assertEquals("1, faubourg du pre", NomVoie.Voie("1, fG. du pre"));
		}
		
//========================== Place ================================	
		
		@Test
		public void testplace() {
			assertEquals("6, place stan", NomVoie.Voie("6, place stan"));
		}
		
		@Test
		public void testPLACE() {
			assertEquals("6, place stan", NomVoie.Voie("6, PLACE stan"));
		}
		
		@Test
		public void testPlace() {
			assertEquals("6, place stan", NomVoie.Voie("6, Place stan"));
		}
		
		@Test
		public void testpLACE() {
			assertEquals("6, place stan", NomVoie.Voie("6, pLACE stan"));
		}
		
		@Test
		public void testlace() {
			assertEquals("6, place stan", NomVoie.Voie("6, lace stan"));
		}
		
		@Test
		public void testLACE() {
			assertEquals("6, place stan", NomVoie.Voie("6, LACE stan"));
		}
		
		@Test
		public void testLace() {
			assertEquals("6, place stan", NomVoie.Voie("6, Lace stan"));
		}
		
		@Test
		public void testlACE() {
			assertEquals("6, place stan", NomVoie.Voie("6, lACE stan"));
		}
		
		@Test
		public void testplac() {
			assertEquals("6, place stan", NomVoie.Voie("6, plac stan"));
		}
		
		@Test
		public void testPLAC() {
			assertEquals("6, place stan", NomVoie.Voie("6, PLAC stan"));
		}
		
		@Test
		public void testPlac() {
			assertEquals("6, place stan", NomVoie.Voie("6, Plac stan"));
		}
		
		@Test
		public void testpLAC() {
			assertEquals("6, place stan", NomVoie.Voie("6, pLAC stan"));
		}
		
		@Test
		public void testpla() {
			assertEquals("6, place stan", NomVoie.Voie("6, pla stan"));
		}
		
		@Test
		public void testPLA() {
			assertEquals("6, place stan", NomVoie.Voie("6, PLA stan"));
		}
		
		@Test
		public void testPla() {
			assertEquals("6, place stan", NomVoie.Voie("6, Pla stan"));
		}
		
		@Test
		public void testpLA() {
			assertEquals("6, place stan", NomVoie.Voie("6, pLA stan"));
		}
		
		@Test
		public void testpla2() {
			assertEquals("6, place stan", NomVoie.Voie("6, pla. stan"));
		}
		
		@Test
		public void testPLA2() {
			assertEquals("6, place stan", NomVoie.Voie("6, PLA. stan"));
		}
		
		@Test
		public void testPla2() {
			assertEquals("6, place stan", NomVoie.Voie("6, Pla. stan"));
		}
		
		@Test
		public void testpLA2() {
			assertEquals("6, place stan", NomVoie.Voie("6, pLA. stan"));
		}
		
		@Test
		public void testplc() {
			assertEquals("6, place stan", NomVoie.Voie("6, plc stan"));
		}
		
		@Test
		public void testPLC() {
			assertEquals("6, place stan", NomVoie.Voie("6, PLC stan"));
		}
		
		@Test
		public void testPlc() {
			assertEquals("6, place stan", NomVoie.Voie("6, Plc stan"));
		}
		
		@Test
		public void testpLC() {
			assertEquals("6, place stan", NomVoie.Voie("6, pLC stan"));
		}
		
		@Test
		public void testplc2() {
			assertEquals("6, place stan", NomVoie.Voie("6, plc. stan"));
		}
		
		@Test
		public void testPLC2() {
			assertEquals("6, place stan", NomVoie.Voie("6, PLC. stan"));
		}
		
		@Test
		public void testPlc2() {
			assertEquals("6, place stan", NomVoie.Voie("6, Plc. stan"));
		}
		
		@Test
		public void testpLC2() {
			assertEquals("6, place stan", NomVoie.Voie("6, pLC. stan"));
		}
		
		@Test
		public void testpl() {
			assertEquals("6, place stan", NomVoie.Voie("6, pl stan"));
		}
		
		@Test
		public void testPL() {
			assertEquals("6, place stan", NomVoie.Voie("6, PL stan"));
		}
		
		@Test
		public void testPl() {
			assertEquals("6, place stan", NomVoie.Voie("6, Pl stan"));
		}
		
		@Test
		public void testpL() {
			assertEquals("6, place stan", NomVoie.Voie("6, pL stan"));
		}
		
		@Test
		public void testpl2() {
			assertEquals("6, place stan", NomVoie.Voie("6, pl. stan"));
		}
		
		@Test
		public void testPL2() {
			assertEquals("6, place stan", NomVoie.Voie("6, PL. stan"));
		}
		
		@Test
		public void testPl2() {
			assertEquals("6, place stan", NomVoie.Voie("6, Pl. stan"));
		}
		
		@Test
		public void testpL2() {
			assertEquals("6, place stan", NomVoie.Voie("6, pL. stan"));
		}
}
