package controller;

import java.net.URL;
import java.util.ResourceBundle;

import application.VueDuree;
import dao.DAOFactory;
import dao.DAOManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import metier.Duree;
import metier.Periodicite;
import javafx.scene.control.TableView;

import javafx.scene.control.TableColumn;

public class gestiondureeController implements Initializable {
	@FXML
	private TableView<Duree> txtf_duree;
	@FXML
	private TableColumn<Duree,String> cln_form;
	@FXML
	private TextField txtf_form;
	@FXML
	private Button btn_add;
	@FXML
	private Button btn_prec;
	@FXML
	private Button btn_suppr;
	private VueDuree vue;
	private Duree modele;
 
	public void setVue(VueDuree vueDuree){
		this.vue=vueDuree;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		DAOFactory dao = DAOManager.getCurrentFactory();
		
		this.btn_suppr.setDisable(true);
		
		this.txtf_duree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			 this.btn_suppr.setDisable(newValue == null);
			 });
		
		this.cln_form =new TableColumn<Duree, String>("Libell�");
		this.cln_form.setCellValueFactory(new PropertyValueFactory<Duree, String>("libelleFormule"));
		this.txtf_duree.getColumns().setAll(cln_form);
		this.txtf_duree.getItems().addAll(dao.getDureeDAO().findAll());
	}
	@FXML
	public void addDuree(ActionEvent event) {
		// TODO Autogenerated
		String erreur = "";
		modele = new Duree();
		String libel = this.txtf_form.getText().trim();
        try {
            modele.setLibelleFormule(libel);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
		if(erreur!=""){
		Alert alert=new Alert(Alert.AlertType.ERROR);
		alert.initOwner(this.vue);
		alert.setTitle("Erreur lors de la saisie");
		alert.setHeaderText("Un ou plusieurs champs sont mal remplis.");
		alert.setContentText(erreur);
		alert.showAndWait();}
		else{
			DAOManager.getCurrentFactory().getDureeDAO().create(modele);
			this.txtf_duree.getItems().add(modele);
			}
		}
	
	@FXML
	public void close(ActionEvent event) {
		// TODO Autogenerated
		this.vue.close();
	}
	// Event Listener on Button.onAction
	
	@FXML
	public void supprimer(ActionEvent event) {
		// TODO Autogenerated
		DAOManager.getCurrentFactory().getDureeDAO().delete(this.txtf_duree.getSelectionModel().getSelectedItem());
		this.txtf_duree.getItems().remove(this.txtf_duree.getSelectionModel().getSelectedItem());
	}
}
