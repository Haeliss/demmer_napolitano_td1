package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import metier.Client;
import metier.Periodicite;
import metier.Revue;

import java.net.URL;
import java.util.ResourceBundle;

import application.VueClients;
import dao.DAOFactory;
import dao.DAOManager;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class GestionClientController implements Initializable{
	private VueClients vue;
	@FXML
	private TableView<Client> tableClient;
	@FXML
	private Button creer;
	@FXML
	private Button fermer;
	@FXML
	private Button supprimer;
	@FXML
	private TextField nom;
	@FXML
	private TextField prenom;
	@FXML
	private TextField noRue;
	@FXML
	private TextField nomRue;
	@FXML
	private TextField postale;
	@FXML
	private TextField pays;
	@FXML
	private TextField ville;
	@FXML
	private TableColumn<Client, String> colNom;
	@FXML
	private TableColumn<Client, String> colPrenom;
	@FXML
	private TableColumn<Client, String> colNoRue;
	@FXML
	private TableColumn<Client, String> colNomRue;
	@FXML
	private TableColumn<Client, String> colPostale;
	@FXML
	private TableColumn<Client, String> colVille;
	@FXML
	private TableColumn<Client, String> colPays;
	private Client modele;
	
	public void initialize(URL location, ResourceBundle resources) {
		DAOFactory dao = DAOManager.getCurrentFactory();
		
		this.supprimer.setDisable(true);
		
		this.tableClient.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			 this.supprimer.setDisable(newValue == null);
			 });
		
		this.colNom =new TableColumn<Client, String>("Nom");
		this.colNom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
		this.colPrenom =new TableColumn<Client, String>("Prenom");
		this.colPrenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
		this.colNoRue =new TableColumn<Client, String>("No Rue");
		this.colNoRue.setCellValueFactory(new PropertyValueFactory<Client, String>("norue"));
		this.colNomRue =new TableColumn<Client, String>("Nom Rue");
		this.colNomRue.setCellValueFactory(new PropertyValueFactory<Client, String>("voie"));
		this.colPostale =new TableColumn<Client, String>("Code Postal");
		this.colPostale.setCellValueFactory(new PropertyValueFactory<Client, String>("postale"));
		this.colVille =new TableColumn<Client, String>("Ville");
		this.colVille.setCellValueFactory(new PropertyValueFactory<Client, String>("ville"));
		this.colPays =new TableColumn<Client, String>("Pays");
		this.colPays.setCellValueFactory(new PropertyValueFactory<Client, String>("pays"));
		
		this.tableClient.getColumns().setAll(colNom, colPrenom, colNoRue, colNomRue, colPostale, colVille, colPays);
		this.tableClient.getItems().addAll(dao.getClientDAO().findAll());

	}
	// Event Listener on Button.onAction
	@FXML
	public void addClient(ActionEvent event) {
		String erreur = "";
		modele = new Client();
		String  nom= this.nom.getText().trim();
        String prenom = this.prenom.getText().trim();
        String norue = this.noRue.getText().trim();
        String voie = this.nomRue.getText().trim();
        String postale = this.postale.getText().trim();
        String ville = this.ville.getText().trim();
        String pays = this.pays.getText().trim();
        try {
            modele.setNom(nom);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        try {
            modele.setPrenom(prenom);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        try {
            modele.setNorue(norue);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        try {
            modele.setVoie(voie);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        try {
            modele.setPostale(postale);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        try {
            modele.setVille(ville);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        try {
            modele.setPays(pays);
            
        } catch (IllegalArgumentException iae) {
        	erreur=erreur+iae.getMessage()+"\n";
        }
        
		if(erreur!=""){
		Alert alert=new Alert(Alert.AlertType.ERROR);
		alert.initOwner(this.vue);
		alert.setTitle("Erreur lors de la saisie");
		alert.setHeaderText("Un ou plusieurs champs sont mal remplis.");
		alert.setContentText(erreur);
		alert.showAndWait();}
		else{
			DAOManager.getCurrentFactory().getClientDAO().create(modele);
			this.tableClient.getItems().add(modele);
			}
	}
	// Event Listener on Button.onAction
	@FXML
	public void close(ActionEvent event) {
		// TODO Autogenerated
		this.vue.close();
	}
	// Event Listener on Button.onAction
	@FXML
	public void supprimer(ActionEvent event) {
		// TODO Autogenerated
		DAOManager.getCurrentFactory().getClientDAO().delete(this.tableClient.getSelectionModel().getSelectedItem());
		this.tableClient.getItems().remove(this.tableClient.getSelectionModel().getSelectedItem());
	}
	public void setVue(VueClients vueClients) {
		// TODO Auto-generated method stub
		this.vue=vueClients;
	}
}
