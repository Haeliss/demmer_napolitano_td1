package metier;

import java.util.HashMap;

public class Client {
	private int idclient;
	private String nom, prenom, norue, voie, postale, ville, pays;
	private HashMap<Revue, DatesAbonnement> LesAbos= new HashMap<>();
	
	public Client() {}
	
	public Client(String nom, String prenom, String norue, String voie, String postale, String ville, String pays) {
		this.nom = nom;
		this.prenom = prenom;
		this.norue = norue;
		this.voie = voie;
		this.postale = postale;
		this.ville = ville;
		this.pays = pays;
	}

	public Client(int idClient, String nom, String prenom, String norue, String voie, String postale, String ville,
			String pays, HashMap<Revue, DatesAbonnement> lesAbos) {
		this.idclient = idClient;
		this.nom = nom;
		this.prenom = prenom;
		this.norue = norue;
		this.voie = voie;
		this.postale = postale;
		this.ville = ville;
		this.pays = pays;
		LesAbos = lesAbos;
	}
	public Client(int idClient, String nom, String prenom, String norue, String voie, String postale, String ville,
			String pays) {
		super();
		this.idclient = idClient;
		this.nom = nom;
		this.prenom = prenom;
		this.norue = norue;
		this.voie = voie;
		this.postale = postale;
		this.ville = ville;
		this.pays = pays;
		this.LesAbos = new HashMap<Revue, DatesAbonnement>();
	}

	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idClient) {
		this.idclient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		if(nom==null||nom.length()==0){
			throw new IllegalArgumentException("Le nom est vide !");
		}
		else this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		if(prenom==null||prenom.length()==0){
			throw new IllegalArgumentException("Le pr�nom est vide !");
		}
		else this.prenom = prenom;
	}

	public String getNorue() {
		return norue;
	}

	public void setNorue(String norue) {
		if(norue==null||norue.length()==0){
			throw new IllegalArgumentException("Le num�ro de rue est vide !");
		}
		else this.norue = norue;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		if(voie==null||voie.length()==0){
			throw new IllegalArgumentException("La voie est vide !");
		}
		else this.voie = voie;
	}

	public String getPostale() {
		return postale;
	}

	public void setPostale(String postale) {
		if(postale==null||postale.length()==0){
			throw new IllegalArgumentException("Le code postal est vide !");
		}
		else this.postale = postale;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		if(ville==null||ville.length()==0){
			throw new IllegalArgumentException("La ville est vide !");
		}
		else this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		if(pays==null||pays.length()==0){
			throw new IllegalArgumentException("Le pays est vide !");
		}
		else this.pays = pays;
	}

	public HashMap<Revue, DatesAbonnement> getLesAbos() {
		return LesAbos;
	}

	public void setLesAbos(HashMap<Revue, DatesAbonnement> lesAbos) {
		LesAbos = lesAbos;
	}
	public boolean equals(Object o) {

		Client c =(Client)o;

		return this.idclient==c.idclient;

		}
	public int hashCode(){
		 int hashCode =  17 ;   
		    hashCode =  31 * hashCode + idclient ;
		     return hashCode ;
	}
	public String toString(){
		return "Nom: "+this.nom+", Prenom: "+this.prenom;
	}
}
