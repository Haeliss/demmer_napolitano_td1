package metier;

public class DatesAbonnement {
	private String dateDebut, dateFin;
	private Duree duree;
	public DatesAbonnement(String dateDebut, String dateFin, Duree duree) {
		dateDebut = dateDebut;
		dateFin = dateFin;
		this.duree = duree;
	}
	
	public DatesAbonnement() {}

	public String getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(String dateDebut) {
		if(dateDebut==null||dateDebut.length()==0){
			throw new IllegalArgumentException("La date de d�but est vide !");
		}
		else if (!dateDebut.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
			throw new IllegalArgumentException("Erreur de format de la date de d�but !");}
		else this.dateDebut = dateDebut;
	}
	public String getDateFin() {
		return dateFin;
	}
	public void setDateFin(String dateFin) {
		if(dateFin==null||dateFin.length()==0){
			throw new IllegalArgumentException("La date de fin est vide !");
		}
		else if (!dateFin.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
			throw new IllegalArgumentException("Erreur de format de la date de fin !");}
		else this.dateFin = dateFin;
	}
	public Duree getDuree() {
		return duree;
	}
	public void setDuree(Duree duree) {
		this.duree = duree;
	}
	public boolean equals(Object o) {

		DatesAbonnement a =(DatesAbonnement)o;

		return ((this.duree==a.duree)&&(this.dateDebut==a.dateDebut)&&(this.dateFin==a.dateFin));

		}
	public int hashCode(){
		 int hashCode =  17 ; 
		 hashCode =  31 * hashCode + ((dateDebut == null) ?  0 : dateDebut.hashCode()) ;
		 hashCode =  31 * hashCode + ((dateFin == null) ?  0 : dateFin.hashCode()) ;
		 hashCode =  31 * hashCode + ((duree == null) ?  0 : duree.hashCode()) ;
		 return hashCode ;
	}
	public String toString(){
		return "Date debut: "+this.dateDebut+", Date fin: "+this.dateFin;
	}
}
