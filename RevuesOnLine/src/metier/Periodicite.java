package metier;

public class Periodicite {
	private int id;
	private String libel;
	public Periodicite(int id, String libel) {
		this.id = id;
		this.libel = libel;
	}
	
	public Periodicite(String libel) {
		super();
		this.libel = libel;
	}


	public Periodicite() {}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibel() {
		return libel;
	}
	public void setLibel(String libel) {
		if(libel==null||libel.length()==0){
			throw new IllegalArgumentException("Le libell� est vide !");
		}
		else this.libel = libel;
	}
	public boolean equals(Object o) {
		if(o==null)return false;
		Periodicite p =(Periodicite)o;

		return this.id==p.id;

		}
	public int hashCode(){
		 int hashCode =  17 ;   
		    hashCode =  31 * hashCode + id ;
		     return hashCode ;
	}
	public String toString(){
		return this.libel;
	}
}
