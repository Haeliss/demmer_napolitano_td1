package metier;

public class Duree {
	private int idDuree; 
	private String libelleFormule;
	
	public Duree(int idDuree, String libelleFormule) {
		this.idDuree = idDuree;
		this.libelleFormule = libelleFormule;
	}
	
	public Duree() {}

	public Duree(String libelleFormule) {
		super();
		this.libelleFormule = libelleFormule;
	}


	public int getIdDuree() {
		return idDuree;
	}

	public void setIdDuree(int idDuree) {
		this.idDuree = idDuree;
	}

	public String getLibelleFormule() {
		return libelleFormule;
	}

	public void setLibelleFormule(String libelleFormule) {
		if(libelleFormule==null||libelleFormule.length()==0){
			throw new IllegalArgumentException("Le libell� est vide !");
		}
		else this.libelleFormule = libelleFormule;
	}
	public boolean equals(Object o) {

		Duree d =(Duree)o;

		return this.idDuree==d.idDuree;

		}
	public int hashCode(){
		 int hashCode =  17 ;   
		    hashCode =  31 * hashCode + idDuree ;
		     return hashCode ;
	}
	public String toString(){
		return this.libelleFormule;
	}
}
