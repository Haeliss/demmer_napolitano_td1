package metier;

public class Formule {
	private Revue revue;
	private Duree duree;
	private float reduction;
	
	public Formule(Revue revue, Duree duree, float reduction) {
		super();
		this.revue = revue;
		this.duree = duree;
		this.reduction = reduction;
	}
	
	public Formule() {
		super();
	}

	public Revue getRevue() {
		return revue;
	}

	public void setRevue(Revue revue) {
		this.revue = revue;
	}

	public Duree getDuree() {
		return duree;
	}

	public void setDuree(Duree duree) {
		this.duree = duree;
	}

	public float getReduction() {
		return reduction;
	}

	public void setReduction(float reduction) {
		this.reduction = reduction;
	}
	public boolean equals(Object o) {

		Formule f =(Formule)o;

		return ((this.duree==f.duree)&&(this.revue==f.revue)&&(this.reduction==f.reduction));

		}
	public int hashCode(){
		 int hashCode =  17 ; 
		 hashCode =  31 * hashCode + ((revue == null) ?  0 : revue.hashCode()) ;
		 hashCode =  31 * hashCode + Float.floatToIntBits(reduction) ;
		 hashCode =  31 * hashCode + ((duree == null) ?  0 : duree.hashCode()) ;
		 return hashCode ;
	}
	public String toString(){
		return this.revue.toString()+", "+this.duree.toString()+", reduction: "+this.reduction;
	}
}
