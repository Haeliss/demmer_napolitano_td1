package metier;

import java.util.HashMap;

public class Revue {
	private int idrevue;
	private String titre, desc, visuel;
	private float tarif;
	private Periodicite period;
	private HashMap<Duree,Float> Formule= new HashMap<>();

	
	public Revue() {}

	public Revue(String titre, String desc, float tarif, Periodicite period) {
		super();
		this.titre = titre;
		this.desc = desc;
		this.tarif = tarif;
		this.period = period;
	}

	public Revue(int idRevue, String titre, String desc, String visuel, float tarif, Periodicite period,
			HashMap<Duree, Float> formule) {
		this.idrevue = idRevue;
		this.titre = titre;
		this.desc = desc;
		this.visuel = visuel;
		this.tarif = tarif;
		this.period = period;
		Formule = formule;
	}
	
	public Revue(int idRevue, String titre, String desc, String visuel, float tarif, Periodicite period) {
		super();
		this.idrevue = idRevue;
		this.titre = titre;
		this.desc = desc;
		this.visuel = visuel;
		this.tarif = tarif;
		this.period = period;
		this.Formule = new HashMap<Duree, Float>();
	}

	public int getIdrevue() {
		return idrevue;
	}
	public void setIdrevue(int idRevue) {
		this.idrevue = idRevue;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		if(titre==null||titre.length()==0){
			throw new IllegalArgumentException("Le titre est vide !");
		}
		else this.titre = titre;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		if(desc==null||desc.length()==0){
			throw new IllegalArgumentException("La description est vide !");
		}
		else this.desc = desc;
	}
	public String getVisuel() {
		return visuel;
	}
	public void setVisuel(String visuel) {
		this.visuel = visuel;
	}
	public float getTarif() {
		return tarif;
	}
	public void setTarif(float tarif) {
		this.tarif = tarif;
	}
	public Periodicite getPeriod() {
		return period;
	}
	public void setPeriod(Periodicite period) {
		this.period = period;
	}
	public HashMap<Duree, Float> getFormule() {
		return Formule;
	}
	public void setFormule(HashMap<Duree, Float> formule) {
		Formule = formule;
	}
	public boolean equals(Object o) {

		Revue r =(Revue)o;

		return this.idrevue==r.idrevue;

		}
	public int hashCode(){
		 int hashCode =  17 ;   
		    hashCode =  31 * hashCode + idrevue ;
		     return hashCode ;
	}
	public String toString(){
		this.tarif=Math.round(tarif*100);
		this.tarif=this.tarif/100;
		return this.titre+" ("+this.tarif+" euros)";
	}
}
